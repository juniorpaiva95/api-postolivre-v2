<?php

namespace Infrastructure\Http;

use Illuminate\Http\Request;
use Infrastructure\Services\IService;

class CrudController extends Controller
{
    protected $user = null;

	public function __construct(IService $service){
		$this->service = $service;
		$this->user = auth()->user();
	}

	public function index()
	{
        $limit = app('request')->query('limit');
        $items = $limit ?
        $this->service->paginate($limit) : $this->service->all();
        return $this->response($items);
    }


	public function store(Request $request)
	{
		$data = $request->all();
		$item = $this->service->create($data);

		return $this->response($item);
	}

	public function show($id)
	{
	    $item = $this->service->find($id);

		return $this->response($item);
	}

	public function update($id, Request $request)
	{
	    $item = $this->service->update($request->all(), $id);

		return $this->response($item);
	}

	public function destroy($id)
	{
		return $this->response(!!$this->service->delete($id));
	}

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
	public function restore($id)
    {
        return $this->response($this->service->restore($id));
    }
}

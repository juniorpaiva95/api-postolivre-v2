<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 29/03/18
 * Time: 23:44
 */

namespace Infrastructure\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\Config;

class EnforceTenancy
{
    public function handle($request, Closure $next)
    {
        Config::set('database.default', 'tenant');

        return $next($request);
    }
}

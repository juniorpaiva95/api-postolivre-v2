<?php

use Illuminate\Container\Container;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

use Prettus\Validator\Exceptions\ValidatorException;
use Symfony\Component\HttpFoundation\Response;


if (! function_exists('auth')) {
    /**
     * Get the available container instance.
     * @return \Illuminate\Foundation\Application|mixed
     * @internal param string $abstract
     * @internal param array $parameters
     */
    function auth()
    {
        return Container::getInstance()->make(\Infrastructure\Auth\Helpers\IAuth::class);
    }
}

if (! function_exists('entity')) {
    function entity($class)
    {
        return class_basename($class);
    }
}

if (! function_exists('is_uuid')) {
    function is_uuid($value)
    {
        return preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i', $value);
    }
}
if (! function_exists('website')) {
    function website()
    {
        try{
            return TenancyFacade::website();
        }catch(Exception $ex){
            return null;
        }
    }
}

if (! function_exists('fqdn')) {
    function fqdn()
    {
        try{
            $hostname = TenancyFacade::hostname();
            if(!empty($hostname))
                return explode('.', $hostname->fqdn)[0];

        }catch(Exception $ex){

        }

        return null;
    }
}

if (! function_exists('room')) {
    function task_room($task_id = null)
    {
        if($task_id)
            return fqdn() . '-task-room.{id}';

        return fqdn() . '-task-room.' . $task_id;
    }
}

if (! function_exists('set_env')) {
    function set_env($hostname = null)
    {
        if($hostname) {
            $environment = Container::getInstance()->make(Environment::class);
            $environment->hostname($hostname);
        }
    }
}

if (! function_exists('format_time')) {
    function format_time($timeString, $format = 'H:i')
    {
        return Carbon::createFromTimeString($timeString)->format($format);
    }
}

if (! function_exists('upload_path')) {
    /**
     * Get the path to the storage folder.
     *
     * @param  string $path
     * @param bool $is_public
     * @return string
     */
    function upload_path($path = '', $is_public = true)
    {
        if(!$is_public)
            return storage_path('app/uploads').($path ? DIRECTORY_SEPARATOR. $path : $path);

        return storage_path('app/public/uploads').($path ? DIRECTORY_SEPARATOR. $path : $path);
    }
}

if (! function_exists('is_image')) {
    /**
     * Get the path to the storage folder.
     *
     * @param $file
     * @return string
     * @internal param string $path
     */
    function is_image($file)
    {
        return substr($file->getMimeType(), 0, 5) == 'image';
    }
}

if (! function_exists('ext_from_mime')) {
    /**
     * Get the path to the storage folder.
     *
     * @param string $mimeType
     * @return string
     * @internal param string $path
     */
    function ext_from_mime($mimeType = '')
    {
        $guesser = new \Symfony\Component\Mime\MimeTypes();

        return '.' . $guesser->getExtensions($mimeType)[0];
    }
}



if (! function_exists('resourceKey')) {

    function resourceKey($class, $plural = true)
    {
        $explore = explodeCapital(class_basename($class), false);
        $result = collect($explore)
            ->reject(function($item){
                return is_null($item) || $item == "";
            })->implode('_');

        $key = strtolower($result);
        return $plural ? \Illuminate\Support\Str::plural($key,2): $key;
    }
}


if (! function_exists('statusCode')) {

    function statusCode($code)
    {
        return Response::$statusTexts[$code];
    }
}


if (! function_exists('validate')) {

    function validate(Request $request, array $rules)
    {
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            throw new ValidatorException($validator->errors());
    }
}

if (! function_exists('validateWithData')) {

    function validateWithData(array $data, array $rules)
    {
        $validator = Validator::make($data, $rules);

        if ($validator->fails())
            throw new ValidatorException($validator->errors());
    }
}


if (! function_exists('error')) {

    function error($exception, $code = 500, $message = null)
    {
        if(is_null($message)){
            $message = empty($exception->getMessage()) ? explodeCapital(entity($exception)) : $exception->getMessage();
        }

        if(method_exists($exception, 'getMessageBag')){
            return ['error' => [
                'code' => 422,
                'message' => $message,
                'errors'  => $exception->getMessageBag()
            ]];
        }else {
            return ['error' => [
                'code' => $code,
                'message' => $message
            ]];
        }
    }
}


if (! function_exists('explodeCapital')) {

    function explodeCapital($string, $joinAfter = true)
    {
        $value = preg_split('/(?=[A-Z])/', $string);
        return $joinAfter ? join(' ' ,$value) : $value;
    }
}


if (! function_exists('array_remove_null')) {
    function array_remove_null($item)
    {
        if (!is_array($item)) {
            return $item;
        }

        return collect($item)
            ->reject(function ($item) {
                return is_null($item);
            })
            ->flatMap(function ($item, $key) {
                if(is_numeric($key)){
                    return [array_remove_null($item)];
                }else{
                    return [$key => array_remove_null($item)];
                }
            })
            ->toArray();
    }
}



<?php


namespace Infrastructure\Exceptions;

use Exception;

interface ReporterInterface
{
    public function report(Exception $e);
}

<?php

namespace Infrastructure\Exceptions\Formatters;

use Exception;
use Illuminate\Http\JsonResponse;
use Infrastructure\Exceptions\BaseFormatter;

class UnprocessableEntityHttpExceptionFormatter extends BaseFormatter
{
	public function format(JsonResponse $response, Exception $ex, array $reporterResponses)
	{
        $statusCode = 422;

        $data = [
            'code'    => empty($ex->getCode()) ? 422 : $ex->getCode(),
            'message' => trans('exceptions.validation'),
            'errors'  => json_decode($ex->getMessage())
        ];

        $response->setStatusCode($statusCode);

        $this->debug ?
            $response->setData(['error' => $data ]) :
            $response->setData(error($ex, $statusCode, trans('exceptions.generic')));
        ;
	}
}

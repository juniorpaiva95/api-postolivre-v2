<?php

namespace Infrastructure\Exceptions\Formatters;

use Exception;
use Illuminate\Http\JsonResponse;
use Infrastructure\Exceptions\BaseFormatter;

class ValidatorExceptionFormatter extends BaseFormatter
{
	public function format(JsonResponse $response, Exception $ex, array $reporterResponses)
	{
        $response->setStatusCode(422);
        $response->setData(error($ex, 422, trans('exceptions.validation_error')));
	}
}

<?php
namespace Infrastructure\Exceptions\Formatters;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Infrastructure\Exceptions\BaseFormatter;
use Symfony\Component\HttpFoundation\Response;

class ExceptionFormatter extends BaseFormatter
{
    public function format(JsonResponse $response, Exception $ex, array $reporterResponses)
    {

        $statusCode = !Arr::has(Response::$statusTexts, $ex->getCode()) ? 500 : $ex->getCode();

        $response->setStatusCode($statusCode);

        $this->debug ?
            $response->setData(error($ex, $statusCode)):
            $response->setData(error($ex, $statusCode, trans('exceptions.generic')));
    }
}

<?php
namespace Infrastructure\Exceptions\Formatters;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use Infrastructure\Exceptions\BaseFormatter;

class ModelNotFoundExceptionFormatter extends BaseFormatter
{
    public function format(JsonResponse $response, Exception $ex, array $reporterResponses)
    {
        $statusCode = 404;

        $response->setStatusCode($statusCode);

        $modelName = Str::lower(entity($ex->getModel()));
        $this->debug ?
            $response
                ->setData(error($ex, $statusCode, trans('exceptions.resource_does_not_exist', [
                    'model' => trans("entities.${modelName}"),
                    'key'   => $ex->getIds()[0]
                ]))) :
            $response
                ->setData(error($ex, $statusCode, trans('exceptions.generic')));
    }
}

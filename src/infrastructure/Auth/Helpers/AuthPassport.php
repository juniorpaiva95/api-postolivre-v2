<?php

namespace Infrastructure\Auth\Helpers;

use Api\User\Interfaces\IUserService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Passport\ClientRepository;
use Lcobucci\JWT\Parser;

/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 11/04/18
 * Time: 15:16
 */
class AuthPassport implements IAuth
{
    private $user;
    private $request;
    private $userService;

    function __construct(IUserService $userService)
    {
        $this->request = request();
        $this->userService = $userService;

        if(!is_null($this->request))
            $this->user = request()->user('api');
    }

    public function user(){
        return $this->user ?? $this->getUserFromToken();
    }

    private function getUserFromToken(){
        $bearerToken = $this->request->bearerToken();

        if(is_null($bearerToken)){
            return $bearerToken;
        }

        $clientRepository = new ClientRepository();
        $jwt = (new Parser)->parse($bearerToken);
        $client = $clientRepository->find($jwt->getClaim('aud'));

        if (!is_null($client->user_id)) {
            try {
                return $this->userService->find($client->user_id);
            } catch (ModelNotFoundException $ex) {
                return null;
            }
        }
    }
}
<?php
/**
 * Created by IntelliJ IDEA.
 * User: stalo
 * Date: 19/02/18
 * Time: 11:02
 */

namespace Infrastructure\Auth\Controllers;


use Api\User\Interfaces\IUserService;
use Api\User\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Infrastructure\Http\Controller;

class ResetPasswordController extends Controller
{
    use CanResetPassword;

    protected $service;

	function __construct(IUserService $service)
    {
        $this->service = $service;
    }

    public function reset(Request $request)
    {
        $this->service->runService(function() use ($request){
            validate($request, $this->rules());

            $user = $this->service
                ->findWhere([['email', '=', $request->get('email')]])
                ->first();

            if(!is_null($user)){
                $this->service->update($request->all(), $user->id);
                $response = $this->broker()->reset(
                        $this->credentials($request), function ($user, $password) {
                        $this->resetPassword($user, $password);
                    }
                );

                if($response != Password::PASSWORD_RESET){
                    Log::error(json_encode($response));
                    throw new \Exception('Something went wrong. verify the informations provided.');
                }
            }else{
                throw (new ModelNotFoundException())->setModel(User::class, $request->email);
            }

        }, null);

        return $this->response(['message' => 'Senha alterada com sucesso!!'],200);
    }



    /**
     * Reset the given user's password.
     */
    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);

        $user->setRememberToken(Str::random(60));

        $user->confirmed = true;

        $user->email_verified_at = new \DateTime();

        $user->save();

        event(new PasswordReset($user));
    }

    private function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::getFacadeRoot();
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
    }
}

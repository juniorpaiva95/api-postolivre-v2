<?php

namespace Infrastructure\Auth\Controllers;

use Api\User\Interfaces\IUserService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    private $userService;

    /**
     * RegisterController constructor.
     * @param IUserService $userService
     */
    public function __construct(IUserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param Request $request
     */
    public function register(Request $request) {
        $this->userService->create($request->all());
    }
}

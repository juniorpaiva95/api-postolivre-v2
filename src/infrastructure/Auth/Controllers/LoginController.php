<?php

namespace Infrastructure\Auth\Controllers;

use Api\User\Models\User;
use Illuminate\Http\Request;
use Infrastructure\Auth\LoginProxy;
use Infrastructure\Auth\Requests\ClientLoginRequest;
use Infrastructure\Auth\Requests\LoginRequest;
use Infrastructure\Http\Controller;

class LoginController extends Controller
{
    private $loginProxy;

    public function __construct(LoginProxy $loginProxy)
    {
        $this->loginProxy = $loginProxy;
    }

    public function login(LoginRequest $request)
    {
        $username = $request->get('username'); // Email or cnpj
        $password = $request->get('password');
        $remember = $request->get('remember') ?? false;

        $user = $this->loginProxy->attemptLogin($username, $password, $remember);

        return $this->response($user);
    }

    public function loginClient(Request $request)
    {
        $client_secret = $request->get('token');

        return $this->response($this->loginProxy->attempClientLogin($client_secret));
    }

    public function refresh()
    {
        return $this->response($this->loginProxy->attemptRefresh());
    }

    public function logout()
    {
        $this->loginProxy->logout();

        return $this->response(null, 204);
    }
}

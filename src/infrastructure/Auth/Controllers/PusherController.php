<?php
/**
 * Created by PhpStorm.
 * User: stalo
 * Date: 28/04/19
 * Time: 20:12
 */

namespace Infrastructure\Auth\Controllers;


use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Infrastructure\Http\Controller;
use Pusher\Pusher;

class PusherController extends Controller
{

    //accessed through '/pusher/'
    //setup your routes.php accordingly

    public function __construct() {
        parent::__construct();
        //Let's register our pusher application with the server.
        //I have used my own config files. The config keys are self-explanatory.
        //You have received these config values from pusher itself, when you signed up for their service.
        $this->pusher = new Pusher(Config::get('pusher.app_key'), Config::get('pusher.app_secret'), Config::get('pusher.app_id'));
    }

    /**
     * Authenticates logged-in user in the Pusher JS app
     * For presence channels
     */
    public function postAuth()
    {
        //We see if the user is logged in our laravel application.
        if(Auth::check())
        {
            //Fetch User Object
            $user =  Auth::user();
            //Presence Channel information. Usually contains personal user information.
            //See: https://pusher.com/docs/client_api_guide/client_presence_channels
            $presence_data = array('name' => $user->first_name." ".$user->last_name);
            //Registers users' presence channel.
            echo $this->pusher->presence_auth(Input::get('channel_name'), Input::get('socket_id'), $user->id, $presence_data);
        }
        else
        {
            return Response::make('Forbidden',403);
        }
    }
}
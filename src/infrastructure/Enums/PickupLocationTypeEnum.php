<?php


namespace Infrastructure\Enums;


class PickupLocationTypeEnum
{
    const PORTO_CABEDELO = 1;
    const PORTO_SUAPE = 2;
    const PORTO_GUAMORE = 3;

    /**
     * @param int $pickupLocationType
     * @return string
     */
    public static function getPresentationName(int $pickupLocationType) : string
    {
        switch ($pickupLocationType) {
            case 1:
                return 'Porto de Cabedelo (PB)';
                break;
            case 2:
                return 'Porto de Suape (PE)';
                break;
            case 3:
                return 'Porto de Guamaré (RN)';
                break;
            default:
                return 'Local de retirada inválido!';
                break;
        }
    }
}

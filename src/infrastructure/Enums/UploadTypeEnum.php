<?php

namespace Infrastructure\Enums;

class UploadTypeEnum
{
    /**
     * @example Representa o upload de imagens no cadastro de posto
     */
    const STATION_PROFILE_PICTURE = 'STATION_PROFILE_PICTURE';

    /**
     * @example Representa o upload de imagens de comprovante de pagamento
     * de solicitações de combustível
     */
    const PAYMENT_VOUCHER = 'PAYMENT_VOUCHER';
}
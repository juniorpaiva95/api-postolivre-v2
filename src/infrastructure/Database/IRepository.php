<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 27/04/18
 * Time: 14:38
 */

namespace Infrastructure\Database;

use Prettus\Repository\Contracts\RepositoryInterface;

interface IRepository extends RepositoryInterface
{
    public function deleteWhere(array $where);
    public function skipCache($status = true);
    public function parserResult($result);

    public function updateWhere(array $where, $data = []);
    public function increment(array $columns);
    public function first();
}

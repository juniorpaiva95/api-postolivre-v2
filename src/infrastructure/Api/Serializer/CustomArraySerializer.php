<?php


namespace Infrastructure\Api\Serializer;


use League\Fractal\Serializer\ArraySerializer;

class CustomArraySerializer extends ArraySerializer
{
    public function null()
    {
        return null;
    }
}

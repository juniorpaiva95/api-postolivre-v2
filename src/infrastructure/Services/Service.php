<?php

namespace Infrastructure\Services;

use Api\User\Models\User;
use Closure;
use Exception;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Database\IRepository;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;

/**
 * Class Service
 * @package Infrastructure\Services
 */
abstract class Service implements IService
{
    protected $repository;
    protected $validator;
    protected $database;
    protected $dispatcher;
    protected $previousState;
    /**
     * @var User|null
     */
    protected $userLogged;

    protected $modelCreated;
    protected $modelUpdated;
    protected $modelDeleted;
    /**
     * @uses Representa o autoincrement do identifier (identificador secundário para o usuário)
     * @var boolean false
     */
    protected $autoIncrement;

    /**
     * Service constructor.
     * @param DatabaseManager $database
     * @param Dispatcher $dispatcher
     * @param IRepository $repository
     */
    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        IRepository $repository) {
        $this->autoIncrement = false;
        $this->repository = $repository;
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userLogged = auth() ?? auth()->user();
    }

    public function paginate($limit = 20)
    {
        return $this->repository->paginate($limit);
    }

    public function all()
    {
        return $this->repository->all();
    }

    public function find($id)
    {
        return $this->repository->find($id);
    }

    public function with($relations)
    {
        $this->repository = $this->repository->with($relations);
        return $this->repository;
    }

    public function findWhere(array $where, $columns = ['*'])
    {
        return $this->repository->findWhere($where, $columns);
    }

    public function findByField($field, $value = null, $columns = ['*'])
    {
        return $this->repository->findByField($field, $value = null, $columns = ['*']);
    }

    public function pushCriteria($mixed)
    {
        $this->repository = $this->repository->pushCriteria($mixed);
        return $this;
    }

    public function popCriteria($mixed)
    {
        $this->repository = $this->repository->popCriteria($mixed);
        return $this;
    }

    public function scopeQuery(Closure $scope)
    {
        $this->repository = $this->repository->scopeQuery($scope);

        return $this;
    }

    public function withTrashed()
    {
        $this->repository = $this->repository->withTrashed();

        return $this;
    }

    public function skipCriteria($mixed)
    {
        $this->repository = $this->repository->skipCriteria($mixed);
        return $this;
    }

    public function skipPresenter($status = true)
    {
        $this->repository->skipPresenter($status);
        return $this->repository;
    }

    public function setPresenter($mixed)
    {
        $this->repository = $this->repository->setPresenter($mixed);
        return $this;
    }

    public function create(array $data, Model $model = null, $relation = null)
    {
        return $this->runService(function() use ($data, $model, $relation)
        {
            if ($this->autoIncrement) {
                $data['identifier'] = $this->repository->increment(['id']);
            }
            return $model ?
                $model->{$relation}()->create($data) :
                $this->repository->create($data);

        }, $this->modelCreated);
    }

    public function updateWhere(array $where, $data = [])
    {
        return $this->runService(function() use ($where, $data)
        {
            return $this->repository->updateWhere($where, $data);

        }, $this->modelUpdated);
    }

    public function update(array $data, $id)
    {
        return $this->runService(function() use ($data, $id)
        {
            return $this->repository->update($data, $id);

        }, $this->modelUpdated);
    }

    public function delete($id)
    {
        return $this->runService(function() use ($id)
        {
            $result = $this->repository->find($id);
            $this->repository->delete($id);

            return $result;

        }, $this->modelDeleted);
    }

    /**
     * @param $id
     * @return mixed
     * @throws Exception
     * @uses Restaura um recurso deletado
     */
    public function restore($id)
    {
        return $this->runService(function () use ($id){
            $model = $this->repository->withTrashed()->find($id);
            $model->restore();

            return $model;
        }, $this->modelUpdated);
    }


    public function deleteWhere(array $where)
    {
        return $this->runService(function() use ($where)
        {
            $result = $this->repository->findWhere($where);
            $this->repository->deleteWhere($where);

            return $result;

        }, $this->modelDeleted);
    }

    protected function logActivity($result, $function){

        $log = clone $result;
        $log['action']       = $function;
        $log['keys']         = json_decode($result);

        return $log;
    }

    public function runService(Closure $closure, $eventModel){

        $this->database->beginTransaction();
        $model = null;

        try {
            $model = $closure();
            $this->dispatch($eventModel, $model);

        } catch (Exception $ex) {
            $this->database->rollBack();
            throw $ex;
        }

        $this->database->commit();
        return $model;
    }

    private function dispatch($eventModel = null, $model = null){

        !$eventModel ?:
            $this->dispatcher->dispatch(
                new $eventModel(
                    $this->logActivity($model, entity($eventModel))
                )
            );
    }

    public function parserResult($result)
    {
        return $this->repository->skipPresenter(false)->parserResult($result);
    }
}

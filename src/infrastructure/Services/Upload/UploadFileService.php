<?php

namespace Infrastructure\Services\Upload;

use Api\Upload\Models\Upload;
use Infrastructure\Services\Upload\Providers\IUploadFileService;
use Infrastructure\Services\Upload\Providers\IUploadFileServiceBuilder;

class UploadFileService implements IUploadFileService
{
    private $fileService;

    public function __construct(IUploadFileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function save()
    {
        $this->fileService->save();
    }

    public function move($upload)
    {
        $this->fileService->move($upload);
    }

    public function delete(Upload $upload)
    {
        $this->fileService->delete($upload);
    }

    static function createBuilder(): IUploadFileServiceBuilder
    {
        return self::createBuilder();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 05/04/18
 * Time: 14:40
 */

namespace Infrastructure\Services\Upload\Providers;

interface IUploadFileServiceBuilder
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getUuid();

    /**
     * @return $this
     * @internal param mixed $id
     */
    public function setUuid();

    /**
     * @return mixed
     */
    public function getUserId();

    /**
     * @return $this
     * @internal param mixed $id
     */
    public function setUserId($user_id);


    /**
     * @return mixed
     */
    public function getTitle();

    /**
     * @param mixed $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * @return mixed
     */
    public function getDescription();

    /**
     * @param mixed $description
     * @return $this
     */
    public function setDescription($description);

    /**
     * @return mixed
     */
    public function getFile();

    /**
     * @param mixed $file
     * @return $this
     */
    public function setFile($file);

    /**
     * @return mixed
     */
    public function getFilename();


    /**
     * @return mixed
     */
    public function getType();

    /**
     * @param mixed $type
     * @return $this
     */
    public function setType($type);

    /**
     * @return mixed
     */
    public function getIsPublic();

    /**
     * @param mixed $is_public
     * @return $this
     */
    public function setIsPublic($is_public);

    /**
     * @return mixed
     */
    public function getWidth();

    /**
     * @param mixed $width
     * @return $this
     */
    public function setWidth($width);

    /**
     * @return mixed
     */
    public function getHeight();

    /**
     * @param mixed $height
     * @return $this
     */
    public function setHeight($height);

    /**
     * @return mixed
     */
    public function getSize();

    /**
     * @return mixed
     */
    public function getMime();

    /**
     * @return mixed
     */
    public function getExtension();


    public function build(): IUploadFileService;
}
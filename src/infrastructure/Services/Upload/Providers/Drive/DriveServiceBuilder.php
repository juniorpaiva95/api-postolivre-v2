<?php

namespace Infrastructure\Services\Upload\Providers\Drive;


class DriveServiceBuilder implements IDriveServiceBuilder
{

    private $id;
    private $filename;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    public function build():IDriveService {
        return new DriveService($this);
    }
}
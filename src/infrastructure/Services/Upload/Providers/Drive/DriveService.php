<?php

namespace Infrastructure\Services\Upload\Providers\Drive;

use Api\Upload\Models\Upload;

class DriveService implements IDriveService
{

    private $id;
    private $filename;


    static function createBuilder():IDriveServiceBuilder
    {
        return new DriveServiceBuilder();
    }

    public function __construct(DriveServiceBuilder $builder)
    {
        $this->id = $builder->getId();
        $this->filename = $builder->getFilename();
    }

    public function save()
    {
        throw new \Exception('Not Implemented');
    }

    public function delete(Upload $upload)
    {
        throw new \Exception('Not Implemented');
    }
}
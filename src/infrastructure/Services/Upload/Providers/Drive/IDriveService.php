<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 05/04/18
 * Time: 14:08
 */

namespace Infrastructure\Services\Upload\Providers\Drive;

use Api\Upload\Models\Upload;

interface IDriveService
{
    public function save();
    public function delete(Upload $upload);
    static function createBuilder():IDriveServiceBuilder;
}
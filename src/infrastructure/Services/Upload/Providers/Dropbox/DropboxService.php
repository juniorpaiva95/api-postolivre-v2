<?php

namespace Infrastructure\Services\Upload\Providers\Dropbox;

use Api\Upload\Models\Upload;

class DropboxService implements IDropboxService
{

    private $id;
    private $filename;


    static function createBuilder():IDropboxServiceBuilder
    {
        return new DropboxServiceBuilder();
    }

    public function __construct(DropboxServiceBuilder $builder)
    {
        $this->id = $builder->getId();
        $this->filename = $builder->getFilename();
    }

    public function save()
    {
        throw new \Exception('Not Implemented');
    }

    public function delete(Upload $upload)
    {
        throw new \Exception('Not Implemented');
    }
}
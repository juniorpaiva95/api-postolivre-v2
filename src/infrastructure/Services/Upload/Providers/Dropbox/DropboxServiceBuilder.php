<?php

namespace Infrastructure\Services\Upload\Providers\Dropbox;


class DropboxServiceBuilder implements IDropboxServiceBuilder
{

    private $id;
    private $filename;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    public function build():IDropboxService {
        return new DropboxService($this);
    }
}
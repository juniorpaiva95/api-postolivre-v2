<?php

namespace Infrastructure\Services\Upload\Providers\Dropbox;

interface IDropboxServiceBuilder
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getFilename();


    public function build(): IDropboxService;
}
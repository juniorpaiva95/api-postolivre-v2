<?php

namespace Infrastructure\Services\Upload\Providers\Dropbox;

use Api\Upload\Models\Upload;

interface IDropboxService
{
    public function save();
    public function delete(Upload $upload);
    static function createBuilder():IDropboxServiceBuilder;
}
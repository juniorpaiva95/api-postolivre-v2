<?php

namespace Infrastructure\Services\Upload\Providers\Amazon;

use Api\Upload\Interfaces\IUploadService;
use Api\Upload\Models\Upload;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Infrastructure\Services\Upload\Providers\AbstractUploadFileService;
use Infrastructure\Services\Upload\Providers\IUploadFileService;
use Infrastructure\Services\Upload\Providers\IUploadFileServiceBuilder;

class S3UploadFileService extends AbstractUploadFileService implements IUploadFileService
{
    protected $provider =  's3';

    protected $endpoint;
    protected $butcket;

    static function createBuilder(): IUploadFileServiceBuilder
    {
        return new S3UploadFileServiceBuilder();
    }

    public function __construct(IUploadFileServiceBuilder $builder)
    {
        $this->endpoint = config("filesystems.disks.{$this->provider}.url");
        $this->butcket = config("filesystems.disks.{$this->provider}.bucket");

        if(!$this->endpoint || !$this->butcket)
            throw new \Exception('Spaces Provider Configurations Not Found! Verify your enviroments options', 500);

        parent::__construct($builder);
    }

    public function save()
    {
        $url = Storage::disk($this->provider)->putFileAs($this->path(), $this->file, $this->filename, $this->permission[(int)$this->is_public]);
        $url = preg_replace('/(\/+)/','/',$url);

        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'user_id' => $this->user_id,
            'type' => $this->type,
            'title' => $this->title,
            'description' => $this->description,
            'filename' => $this->filename,
            'mime' => $this->mime,
            'extension' => $this->extension,
            'external_url' => $url,
            'external_endpoint' => $this->endpoint,
            'external_bucket' => $this->butcket,
            'provider' => $this->provider,
            'size' => $this->size ,
            'is_public' => (boolean) $this->is_public
        ];
    }

    public function move($upload)
    {
        $filename = $upload->uuid . $upload->extension;
        $path = $this->path($filename);

        if(Storage::disk($this->provider)->exists($path)){
            Storage::disk($this->provider)->setVisibility($path, $this->permission[(int)$upload->is_public]);
        }
    }

    public function delete(Upload $upload)
    {
        $filename = $upload->uuid . $upload->extension;

        $path = $this->path($filename);

        if(Storage::disk($this->provider)->exists($path))
            Storage::disk($this->provider)->delete($path);

    }


    private function path($filename = null)
    {
        return 'tenants' . DIRECTORY_SEPARATOR . fqdn() . DIRECTORY_SEPARATOR . $filename;
    }
}
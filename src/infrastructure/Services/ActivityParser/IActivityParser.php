<?php

namespace Infrastructure\Services\ActivityParser;


use Api\User\Models\Activity;

interface IActivityParser
{
    public function eventType();

    public function getActivity():Activity;

    public function getAvailableEventTypes():array;

    // return the formatted message
    public function parse(): string;

}
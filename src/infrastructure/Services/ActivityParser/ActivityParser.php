<?php

namespace Infrastructure\Services\ActivityParser;

use Api\User\Models\Activity;
use Infrastructure\Services\ActivityParser\Enums\EventTypes;
use Infrastructure\Services\ActivityParser\Parsers\CreationParser;
use Infrastructure\Services\ActivityParser\Parsers\DeletionParser;
use Infrastructure\Services\ActivityParser\Parsers\UnknownParser;
use Infrastructure\Services\ActivityParser\Parsers\UpdateParser;
use ReflectionClass;

class ActivityParser
{
    private $activity;
    private $activityEvent;

    function __construct(Activity $activity)
    {
        $this->activity = $activity;
        $this->activityEvent = new UnknownParser();
    }

    function eventType()
    {
        // interect throught event types
        foreach($this->getAvailableEventTypes() as $type){
            // unsing the event name from db determines which type it is.
            if(str_contains(strtolower($this->getActivity()->description), $type)){
                return $type;
            }
        }

        return null;
    }

    public function parse():string
    {
        // using strategy pattern no set the correct message
        $event = null;

        switch ($this->eventType()){

            case EventTypes::UPDATE:
                $this->activityEvent = new UpdateParser($this->activity);
                break;

            case EventTypes::CREATION:
                $this->activityEvent = new CreationParser($this->activity);
                break;

            case EventTypes::DELETION:
                $this->activityEvent = new DeletionParser($this->activity);
                break;

            default:
                $this->activityEvent = new UnknownParser();
                break;
        }

        return $this->activityEvent->get();
    }

    function getActivity():Activity
    {
        return $this->activity;
    }

    private function getAvailableEventTypes():array
    {
        $refletion = new ReflectionClass(EventTypes::class);
        return $refletion->getConstants();
    }

}
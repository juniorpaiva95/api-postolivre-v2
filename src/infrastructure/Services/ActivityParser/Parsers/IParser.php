<?php

namespace Infrastructure\Services\ActivityParser\Parsers;


interface IParser
{
    // determines where the event occured eg. Task, Subtasks
    function context();

    // determines who trigged the event eg. UserA, UserB
    function subject();

    // determines what entity trigged the event eg. Follow, Upload etc
    function causer();

    // return the formatted message
    public function get(): string;

}
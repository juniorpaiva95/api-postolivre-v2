<?php

namespace Infrastructure\Services\ActivityParser\Parsers;


use Api\Follow\Models\Follow;
use Api\Upload\Models\Upload;
use Api\User\Interfaces\IUserService;
use Api\User\Models\Activity;
use Illuminate\Support\Facades\App;
use Infrastructure\Database\Eloquent\Model;

//TODO: DO A MEGA REFACTOR
class CreationParser extends BaseParser implements IParser
{
    protected $activity;

    function __construct(Activity $activity)
    {
        $this->activity     = $activity;
    }

    function subject()
    {
        // get model
        $subject = App::make($this->activity->subject_type);

        // determines which type is the subject
        switch ($this->activity->subject_type) {

            case Upload::class:
                $subject = $subject->find($this->activity->subject_id);

                $subject = $subject ?? $this->activity->getExtraProperty('entity');

                if(!($subject instanceof Model)){
                    $subject = new Upload($subject);
                    $subject->setAttribute('id', $this->activity->subject_id);
                }

                return $subject->title;

            case Follow::class:
                if ($subject) {
                    $subject = $subject->with('user')->find($this->activity->subject_id);
                    return isset($subject->user) ? $subject->user->name : $this->getUserBySubject()->name;
                }

                break;

            default:
                $subject = $subject->find($this->activity->subject_id);
                return $subject;
        }
    }

    public function get(): string
    {
        $subject = parent::subject();

        $type = $this->setType($subject ?? $this->activity->subject_type);

        switch ($type)
        {
            case 'follow': {
                $index = 'self';
                $sub = $this->getSubject();

                if(auth()->user()->id != $sub['user_id']){
                    $index = 'other';
                }

                return trans("activities/messages.task.{$type}.creation.{$index}", $this->placeholders());
            }
            case 'upload': {
                return trans("activities/messages.task.{$type}.creation.0", $this->placeholders());
            }

            default:
                return trans("activities/messages.task.{$type}.creation.0", $this->placeholders());

        }
    }

    private function placeholders() {

        return [
            'CONTEXT'  => $this->context()['trans'],
            'CAUSER'   => $this->causer()->name,
            'SUBJECT'  => $this->subject(),
        ];
    }

    private function setType($subject)
    {
        return strtolower(entity($subject));
    }

    private function getUserBySubject()
    {
        $subject = $this->getSubject();
        $userService = app(IUserService::class);
        return $userService->scopeQuery(function($query){
            return $query->withTrashed();
        })->find($subject['user_id']);
    }

    private function getSubject()
    {
        $subject = parent::subject();
        return $subject ?  $subject : $this->activity->getExtraProperty('entity');
    }
}
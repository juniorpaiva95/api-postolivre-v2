<?php

namespace Infrastructure\Services\ActivityParser\Parsers;

use Api\User\Models\Activity;

class DeletionParser extends BaseParser implements IParser
{

    protected $activity;

    function __construct(Activity $activity)
    {
        $this->activity = $activity;
    }

    public function get(): string
    {
        $subject = parent::subject();
        $type = strtolower(entity($subject));

        $context = parent::context()['original'];

        if($subject){
            return trans("activities/messages.{$context}.{$type}.deletion.0", $this->placeholders());
        }else{
            return trans("activities/messages.unknown", $this->placeholders());
        }
    }


    private function placeholders() {

        return [
            'CONTEXT'  => $this->context()['trans'],
            'CAUSER'   => $this->causer()->name,
            'SUBJECT'  => "#" . $this->subject(),
        ];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 05/04/18
 * Time: 14:40
 */

namespace Infrastructure\Services\Mail;

interface IMailServiceBuilder
{
    /**
     * @return mixed
     */
    public function getTemplateId();

    /**
     * @param mixed $templateId
     * @return $this
     */
    public function setTemplateId($templateId);

    /**
     * @return mixed
     */
    public function getSubject();

    /**
     * @param $subject
     * @return $this
     * @internal param mixed $templateId
     */
    public function setSubject($subject);

    /**
     * @return mixed
     */
    public function getUser();

    /**
     * @param $user
     * @return $this
     * @internal param mixed $templateId
     */
    public function setUser($user);

    /**
     * @return mixed
     */
    public function getMailableTemplate();

    /**
     * @param $mailableTemplate
     * @return $this
     * @internal param $user
     * @internal param mixed $templateId
     */
    public function setMailableTemplate($mailableTemplate);

    /**
     * @return mixed
     */
    public function getTo();

    /**
     * @param mixed $to
     * @return $this
     */
    public function setTo(array $to);

    /**
     * @return mixed
     */
    public function getFrom();

    /**
     * @param mixed $from
     * @return $this
     */
    public function setFrom(array $from);

    /**
     * @return mixed
     */
    public function getHeaders();

    /**
     * @param mixed $headers
     * @return $this
     */
    public function setHeaders(array $headers);

    /**
     * @return mixed
     */
    public function getAttachment();

    /**
     * @param mixed $attachment
     * @return $this
     */
    public function setAttachment(array $attachment);

    /**
     * @return mixed
     */
    public function getAttachmentUrl();

    /**
     * @param mixed $attachment_url
     * @return $this
     */
    public function setAttachmentUrl($attachment_url);

    /**
     * @return mixed
     */
    public function getAttr();

    /**
     * @param mixed $attr
     * @return $this
     */
    public function setAttr(array $attr);

    /**
     * @return mixed
     */
    public function getReplyto();

    /**
     * @param mixed $replyto
     * @return $this
     */
    public function setReplyto(array $replyto);

    /**
     * @return mixed
     */
    public function getBcc();

    /**
     * @param mixed $bcc
     * @return $this
     */
    public function setBcc(array $bcc);

    /**
     * @return mixed
     */
    public function getCc();

    /**
     * @param mixed $cc
     * @return $this
     */
    public function setCc(array $cc);

    public function build():IMailService;
}
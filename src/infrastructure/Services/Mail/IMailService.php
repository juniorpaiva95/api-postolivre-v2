<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 05/04/18
 * Time: 14:08
 */

namespace Infrastructure\Services\Mail;

interface IMailService
{
    public function send();
    static function createBuilder():IMailServiceBuilder;
}
<?php

namespace Infrastructure\Services\Mail\Providers\SendInBlue;

use Infrastructure\Services\Mail\IMailService;
use Infrastructure\Services\Mail\IMailServiceBuilder;
use Sendinblue\Mailin;

class SendInBlueMailService implements IMailService
{

    private $templateId;
    private $to;
    private $from;
    private $subject;
    private $mailableTemplate;
    private $headers;
    private $attachment;
    private $attachment_url;
    private $attr;
    private $replyto;
    private $bcc;
    private $cc;

    private $mailin;

    public function __construct(SendInBlueMailServiceBuilder $builder)
    {
        $this->templateId = $builder->getTemplateId();
        $this->to = $builder->getTo();
        $this->from = $builder->getFrom();
        $this->subject = $builder->getSubject();
        $this->mailableTemplate = $builder->getMailableTemplate();
        $this->headers = $builder->getHeaders();
        $this->attachment = $builder->getAttachment();
        $this->attachment_url = $builder->getAttachmentUrl();
        $this->attr = $builder->getAttr();
        $this->replyto = $builder->getReplyto();
        $this->bcc = $builder->getBcc();
        $this->cc = $builder->getCc();

        $this->mailin = new Mailin(config('services.sendinblue.url'), config('services.sendinblue.key'));
    }

	public function send()
	{
		$data = [
			"to" => $this->to,
			'from' => $this->from,
			'headers' => $this->headers,
			'attachment_url' => $this->attachment_url,
			"attachment" => $this->attachment,
            "replyto" => $this->replyto,
            "bcc" => $this->bcc,
            "cc" => $this->cc
		];

		if(config('services.sendinblue.enabled')){
            if(NULL == $this->templateId) {
                $data['html'] = $this->mailableTemplate->render();
                $data['subject'] = $this->subject;
                return $this->mailin->send_email($data);
            }else{
                $data['id'] = $this->templateId;
                $data['attr'] = $this->attr;
                return $this->mailin->send_transactional_template($data);
            }
        }

		return false;
	}

    static function createBuilder():IMailServiceBuilder
    {
        return new SendInBlueMailServiceBuilder();
    }
}
<?php

namespace Infrastructure\Services\Mail\Providers\SendInBlue;

use Infrastructure\Services\Mail\IMailService;
use Infrastructure\Services\Mail\IMailServiceBuilder;

class SendInBlueMailServiceBuilder implements IMailServiceBuilder
{

    private $templateId;
    private $mailableTemplate;
    private $user;
    private $subject;
    private $to;
    private $from;
    private $headers;
    private $attachment;
    private $attachment_url;
    private $attr;
    private $replyto;
    private $bcc;
    private $cc;

    /**
     * @return mixed
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * @param mixed $templateId
     * @return $this
     */
    public function setTemplateId($templateId)
    {
        $this->templateId = $templateId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     * @return $this
     */
    public function setTo(array $to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     * @return $this
     */
    public function setFrom(array $from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param mixed $headers
     * @return $this
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @param mixed $attachment
     * @return $this
     */
    public function setAttachment(array $attachment)
    {
        $this->attachment = $attachment;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttachmentUrl()
    {
        return $this->attachment_url;
    }

    /**
     * @param mixed $attachment_url
     * @return $this
     */
    public function setAttachmentUrl($attachment_url)
    {
        $this->attachment_url = $attachment_url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttr()
    {
        return $this->attr;
    }

    /**
     * @param mixed $attr
     * @return $this
     */
    public function setAttr(array $attr)
    {
        $this->attr = $attr;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReplyto()
    {
        return $this->replyto;
    }

    /**
     * @param mixed $replyto
     * @return $this
     */
    public function setReplyto(array $replyto)
    {
        $this->replyto = $replyto;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * @param mixed $bcc
     * @return $this
     */
    public function setBcc(array $bcc)
    {
        $this->bcc = $bcc;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * @param mixed $cc
     * @return $this
     */
    public function setCc(array $cc)
    {
        $this->cc = $cc;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param $user
     * @return $this
     * @internal param mixed $templateId
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMailableTemplate()
    {
        return $this->mailableTemplate;
    }

    /**
     * @param $mailableTemplate
     * @return $this
     * @internal param $user
     * @internal param mixed $templateId
     */
    public function setMailableTemplate($mailableTemplate)
    {
        $this->mailableTemplate = $mailableTemplate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param $subject
     * @return $this
     * @internal param mixed $templateId
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    public function build():IMailService {
        return new SendInBlueMailService($this);
    }
}
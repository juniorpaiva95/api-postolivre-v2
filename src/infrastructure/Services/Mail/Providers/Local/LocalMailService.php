<?php

namespace Infrastructure\Services\Mail\Providers\Local;

use Illuminate\Support\Facades\Mail;
use Infrastructure\Services\Mail\IMailService;
use Infrastructure\Services\Mail\IMailServiceBuilder;

class LocalMailService implements IMailService
{

    private $templateId;
    private $mailableTemplate;
    private $user;
    private $subject;
    private $to;
    private $from;
    private $headers;
    private $attachment;
    private $attachment_url;
    private $attr;
    private $replyto;
    private $bcc;
    private $cc;

    public function __construct(LocalMailServiceBuilder $builder)
    {
        $this->templateId = $builder->getTemplateId();
        $this->user = $builder->getUser();
        $this->subject = $builder->getSubject();
        $this->mailableTemplate = $builder->getMailableTemplate();
        $this->to = $builder->getTo();
        $this->from = $builder->getFrom();
        $this->headers = $builder->getHeaders();
        $this->attachment = $builder->getAttachment();
        $this->attachment_url = $builder->getAttachmentUrl();
        $this->attr = $builder->getAttr();
        $this->replyto = $builder->getReplyto();
        $this->bcc = $builder->getBcc();
        $this->cc = $builder->getCc();
    }

	public function send()
	{
//	    dd($this);
        return Mail::to(key($this->to))->send($this->mailableTemplate);
	}

    static function createBuilder():IMailServiceBuilder
    {
        return new LocalMailServiceBuilder();
    }
}

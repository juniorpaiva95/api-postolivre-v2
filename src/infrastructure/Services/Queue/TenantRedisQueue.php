<?php

namespace Infrastructure\Queue;

use Illuminate\Queue\RedisQueue;
use Illuminate\Support\Facades\Log;
use Predis\Response\ServerException;

/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 31/05/18
 * Time: 10:02
 */
class TenantRedisQueue extends RedisQueue
{
    /**
     * Create a payload array from the given job and data.
     *
     * @param  string  $job
     * @param  mixed   $data
     * @param  string  $queue
     * @return array
     */
    protected function createPayloadArray($job, $data = '', $queue = null)
    {
        $website_id = website() ? website()->id : 0;

        return array_merge(parent::createPayloadArray($job, $data, $queue), [
            'website' => $website_id
        ]);
    }

    /**
     * Pop the next job off of the queue.
     *
     * @param  string  $queue
     * @return \Illuminate\Contracts\Queue\Job|null
     */
    public function pop($queue = null)
    {
        try {
            return parent::pop($queue);
        }
        catch (ServerException $e) {
            Log::critical($e->getMessage());
        }
    }
}
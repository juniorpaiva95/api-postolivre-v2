<?php
/**
 * Created by PhpStorm.
 * User: leonardo
 * Date: 18/09/18
 * Time: 14:27
 */

namespace Infrastructure\Criterias;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class LoggedUserCriteria implements CriteriaInterface
{
    protected $user;

    function __construct($user)
    {
        $this->user = $user;
    }

    public function apply($model, RepositoryInterface $repository)
    {

        if(!is_null($this->user)){
            return ($this->user->is_super) ? $model : $model->where('user_id', '=', $this->user->id);
        }

        return $model;
    }

}
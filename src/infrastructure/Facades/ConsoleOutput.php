<?php

namespace Infrastructure\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static void writeln(string $message)
 *
 * Class ConsoleOutput
 * @package Infrastructure\Facades
 */
class ConsoleOutput extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'consoleOutput';
    }
}

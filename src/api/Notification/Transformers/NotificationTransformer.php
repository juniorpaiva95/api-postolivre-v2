<?php

namespace Api\Notification\Transformers;

use Api\Notification\Models\Notification;
use League\Fractal\TransformerAbstract;

class NotificationTransformer extends TransformerAbstract
{
    protected $availableIncludes = [];

    public function transform(Notification $model)
    {
        return [
            'id'                => $model->id,
            'type'              => $model->type,
            'data'              => json_decode($model->data),
            'read_at'           => $model->read_at,
            'created_at'        => $model->created_at,
            'updated_at'        => $model->updated_at,
//            'deleted_at'        => $model->deleted_at
        ];
    }
}

<?php

namespace Api\Notification;

use Api\Notification\Events\NotificationWasCreated;
use Api\Notification\Events\NotificationWasDeleted;
use Api\Notification\Events\NotificationWasUpdated;
use Api\Notification\Interfaces\INotificationRepository;
use Api\Notification\Interfaces\INotificationService;
use Api\Notification\Repositories\NotificationRepository;
use Api\Notification\Services\NotificationService;
use Infrastructure\Listeners\LogUserActivity;
use Infrastructure\Providers\EventServiceProvider;

class NotificationServiceProvider extends EventServiceProvider
{
    protected $listen = [
        NotificationWasCreated::class => [
            LogUserActivity::class
        ],
        NotificationWasDeleted::class => [
            LogUserActivity::class
        ],
        NotificationWasUpdated::class => [
            LogUserActivity::class
        ],
    ];

    public function register()
    {
        $this->app->bind(INotificationService::class, NotificationService::class);
        $this->app->bind(INotificationRepository::class, NotificationRepository::class);
    }
}

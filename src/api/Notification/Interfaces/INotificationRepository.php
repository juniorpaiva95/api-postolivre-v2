<?php

namespace Api\Notification\Interfaces;

use Infrastructure\Database\IRepository;

interface INotificationRepository extends IRepository
{

}

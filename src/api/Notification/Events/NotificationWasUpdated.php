<?php

namespace Api\Notification\Events;

use Infrastructure\Events\Event;
use Api\Notification\Models\Notification;

class NotificationWasUpdated extends Event
{
    public $notification;

    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }
}

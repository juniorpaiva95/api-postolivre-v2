<?php

namespace Api\Faq\Events;

use Infrastructure\Events\Event;
use Api\Faq\Models\Question;

class QuestionWasUpdated extends Event
{
    public $question;

    public function __construct(Question $question)
    {
        $this->question = $question;
    }
}

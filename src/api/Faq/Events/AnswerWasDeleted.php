<?php

namespace Api\Faq\Events;

use Api\Faq\Models\Answer;
use Infrastructure\Events\Event;

class AnswerWasDeleted extends Event
{
    public $answer;

    public function __construct(Answer $answer)
    {
        $this->answer = $answer;
    }
}
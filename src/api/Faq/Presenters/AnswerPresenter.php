<?php

namespace Api\Faq\Presenters;

use Api\Faq\Transformer\AnswerTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class AnswerPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'answer';
    protected $resourceKeyCollection = 'answers';

    public function getTransformer()
    {
        return new AnswerTransformer();
    }
}

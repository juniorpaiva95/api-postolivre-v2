<?php

namespace Api\Faq\Transformer;

use Api\Faq\Models\Answer;
use Api\Faq\Models\Question;
use Api\User\Models\User;
use Api\User\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

class QuestionTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user', 'answers'];

    public function transform(Question $model)
    {
        return [
            'id' => $model->id,
            'identifier'        => $model->identifier,
            'question' => $model->question,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
            'deleted_at' => $model->deleted_at
        ];
    }

    public function includeUser(Question $model)
    {
        if ($model->user === null) {
            return $this->null();
        }

        return $this->item($model->user, new UserTransformer(), resourceKey(User::class));
    }

    public function includeAnswers(Question $model)
    {
        if (null === $model->answers) {
            return $this->null();
        }

        return $this->collection($model->answers, new AnswerTransformer(), resourceKey(Answer::class));
    }
}

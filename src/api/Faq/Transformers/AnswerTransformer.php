<?php

namespace Api\Faq\Transformer;

use Api\Faq\Models\Answer;
use Api\Faq\Models\Question;
use Api\User\Models\User;
use Api\User\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

class AnswerTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['user', 'question'];

    public function transform(Answer $model)
    {
        return [
            'id' => $model->id,
            'identifier'        => $model->identifier,
            'answer' => $model->answer,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
            'deleted_at' => $model->deleted_at
        ];
    }

    public function includeUser(Answer $model)
    {
        if ($model->user === null) {
            return $this->null();
        }

        return $this->item($model->user, new UserTransformer(), resourceKey(User::class));
    }

    public function includeQuestion(Answer $model)
    {
        if (null === $model->question) {
            return $this->null();
        }

        return $this->item($model->question, new QuestionTransformer(), resourceKey(Question::class));
    }
}

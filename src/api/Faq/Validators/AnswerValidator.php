<?php
namespace Api\Faq\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class AnswerValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'answer'        => 'required|string',
            'user_id'       => 'required|exists:users,id',
            'question_id'   => 'required|exists:faqs_question,id'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'answer'        => 'string',
            'user_id'       => 'required|exists:users,id',
            'question_id'   => 'required|exists:faqs_answer,id'
        ]
    ];

}

<?php
namespace Api\Faq\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class QuestionValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'question'  => 'required|string',
            'user_id'   => 'required|exists:users,id'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'question' => 'string',
            'user_id'   => 'required|exists:users,id'
        ]
    ];

}

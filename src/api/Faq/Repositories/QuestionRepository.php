<?php

namespace Api\Faq\Repositories;

use Api\Faq\Interfaces\IQuestionRepository;
use Api\Faq\Models\Question;
use Api\Faq\Presenters\QuestionPresenter;
use Api\Faq\Validators\QuestionValidator;
use Infrastructure\Database\Eloquent\Repository;


class QuestionRepository extends Repository implements IQuestionRepository
{

    protected $fieldSearchable = [
        'id',
        'question'=>'like',
    ];

    public function model()
    {
        return Question::class;
    }

    public function validator()
    {
        return QuestionValidator::class;
    }

    public function presenter()
    {
        return QuestionPresenter::class;
    }
}

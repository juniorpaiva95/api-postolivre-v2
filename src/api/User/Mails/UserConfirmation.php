<?php

namespace Api\User\Mails;

use Api\User\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UserConfirmation extends Mailable
{
    use Dispatchable, InteractsWithQueue, Queueable;

    protected $user;

    function __construct(User $user)
    {
        $this->user  = $user;
    }

    public function build()
    {
        $url = $this->buildUrl();
        $token = urlencode($this->user->token);

        $logo = base64_encode(file_get_contents(resource_path('app/images/logo-pl.jpg')));
        return $this->view('emails.users.userConfirmation')
//            ->attachFromStorageDisk('resource','images/nilus_cover.png')
            ->subject('Confirmação de Cadastro')
            ->with([
                'userName' => $this->user->social_reason,
                'url' => "{$url}?token={$token}&" . $this->queryStrings($this->user),
                'logo' => $logo
            ]);
    }

    /**
     * Função que retorna a url para reenviar o e-mail de reset de senha
     * @return mixed
     */
    private function buildUrl()
    {
//        if (in_array(env('APP_ENV'), ['local', 'homologacao'])) {
//            return request()->getSchemeAndHttpHost() . ':8080/' . config('services.email.confirmation.link');
//        }

        return env('EMAIL_RESET_PASSWORD_LINK');
    }

    private function queryStrings($user){
        if(empty($user->email))
           throw new \Exception('Required field was not found.');

	    $info = collect($user->toArray())
		    ->only(['email'])
		    ->all();

        return http_build_query($info);
    }
}

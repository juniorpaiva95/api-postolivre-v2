<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 16/01/18
 * Time: 19:13
 */

namespace Api\User\Interfaces;

use Infrastructure\Services\IService;

interface IUserService extends IService
{
    public function sendResetLinkEmail($email);
    public function resendConfirmationEmail($user);

    public function getRoles($id);
    public function getPermissions($id);
    public function getAllPermissions($id);
    public function getUploads($id);
    public function getUploadByType($id, $type);
    public function getUploadedByMe($id);

    public function syncRoles($id, $modelId);
    public function syncPermissions($id, $modelId);

}

<?php

namespace Api\User\Presenters;

use Api\User\Transformers\OauthClientTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class OauthClientPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'oauth-clients';
    protected $resourceKeyCollection = 'oauth-clients';

    public function getTransformer()
    {
        return new OauthClientTransformer();
    }
}
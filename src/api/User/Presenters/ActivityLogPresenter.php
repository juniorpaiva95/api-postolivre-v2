<?php

namespace Api\User\Presenters;

use Api\User\Transformers\ActivityLogTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class ActivityLogPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'activity-logs';
    protected $resourceKeyCollection = 'activity-logs';

    public function getTransformer()
    {
        return new ActivityLogTransformer();
    }
}
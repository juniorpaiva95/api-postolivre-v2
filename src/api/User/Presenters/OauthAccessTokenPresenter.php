<?php

namespace Api\User\Presenters;

use Api\User\Transformers\OauthAccessTokenTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class OauthAccessTokenPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'oauth-access-tokens';
    protected $resourceKeyCollection = 'oauth-access-tokens';

    public function getTransformer()
    {
        return new OauthAccessTokenTransformer();
    }
}
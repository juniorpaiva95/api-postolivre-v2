<?php

namespace Api\User\Presenters;

use Api\User\Transformers\AddressTransformer;
use Api\User\Transformers\BankAccountTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class BankAccountPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'bank_account';
    protected $resourceKeyCollection = 'bank_account';

    public function getTransformer()
    {
        return new BankAccountTransformer();
    }
}
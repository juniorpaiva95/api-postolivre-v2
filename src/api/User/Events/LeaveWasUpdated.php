<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 14/04/18
 * Time: 17:54
 */

namespace Api\User\Events;

use Api\User\Models\AssignmentRole;
use Api\User\Models\Leave;
use Api\User\Models\Team;
use Infrastructure\Events\Event;

class LeaveWasUpdated extends Event
{
    public $leave;

    public function __construct(Leave $leave)
    {
        $this->leave = $leave;
    }
}
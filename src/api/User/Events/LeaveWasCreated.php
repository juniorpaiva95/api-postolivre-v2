<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 14/04/18
 * Time: 17:54
 */

namespace Api\User\Events;

use Api\User\Models\Leave;
use Infrastructure\Events\Event;

class LeaveWasCreated extends Event
{
    public $leave;

    public function __construct(Leave $leave)
    {
        $this->leave = $leave;
    }
}
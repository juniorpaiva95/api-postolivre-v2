<?php

namespace Api\User\Events;

use Hyn\Tenancy\Queue\TenantAwareJob;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Queue\InteractsWithQueue;

class NotificationWasReceived implements ShouldBroadcastNow
{
    use InteractsWithQueue, TenantAwareJob;

    public $user_id;

    public function __construct($user_id)
    {
        $this->user_id= $user_id;
    }

    public function broadcastAs()
    {
        return entity(NotificationWasReceived::class);
    }

    public function broadcastOn()
    {
        return new PresenceChannel($this->roomName());
    }

    private function roomName(){
        return fqdn() . '-notification.' . $this->user_id;
    }
}

<?php

namespace Api\User\Repositories;

use Api\User\Interfaces\IDistributorRepository;
use Api\User\Models\Distributor;
use Api\User\Presenters\DistributorPresenter;
use Infrastructure\Database\Eloquent\Repository;


class DistributorRepository extends Repository implements IDistributorRepository
{

    protected $fieldSearchable = [
        'id',
        'state' =>'like',
        'city'  => 'like',
        'neighborhood'  => 'like',
        'country'   => 'like'
    ];

    public function model()
    {
        return Distributor::class;
    }

    public function validator()
    {
        return null;
    }

    public function presenter()
    {
        return DistributorPresenter::class;
    }
}

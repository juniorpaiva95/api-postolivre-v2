<?php

namespace Api\User\Repositories;

use Api\User\Interfaces\IBankAccountRepository;
use Api\User\Models\Address;
use Api\User\Models\BankAccount;
use Api\User\Presenters\AddressPresenter;
use Api\User\Presenters\BankAccountPresenter;
use Infrastructure\Database\Eloquent\Repository;

class BankAccountRepository extends Repository implements IBankAccountRepository
{

    protected $fieldSearchable = [
        'id',
        'code' =>'like',
        'agency'  => 'like',
        'account'  => 'like',
        'type'   => 'like'
    ];

    public function model()
    {
        return BankAccount::class;
    }

    public function validator()
    {
        return null;
    }

    public function presenter()
    {
        return BankAccountPresenter::class;
    }
}

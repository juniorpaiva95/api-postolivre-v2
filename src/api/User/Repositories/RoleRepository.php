<?php

namespace Api\User\Repositories;

use Api\User\Interfaces\IRoleRepository;
use Api\User\Models\Role;
use Api\User\Presenters\RolePresenter;
use Api\User\Validators\RoleValidator;
use Infrastructure\Database\Eloquent\Repository;

class RoleRepository extends Repository implements IRoleRepository
{

    protected $fieldSearchable = [
        'name' =>'like' ,
        'label' =>'like'
    ];

    public function model()
    {
        return Role::class;
    }

    public function validator()
    {
        return RoleValidator::class;
    }

    public function presenter()
    {
        return RolePresenter::class;
    }

}

<?php

namespace Api\User\Repositories;

use Api\User\Interfaces\IOauthClientRepository;
use Api\User\Models\OauthClient;
use Api\User\Presenters\OauthClientPresenter;
use Api\User\Validators\OauthClientValidator;
use Infrastructure\Database\Eloquent\Repository;

class OauthClientRepository extends Repository implements IOauthClientRepository{

    protected $fieldSearchable = [
        'user_id'
    ];

	public function model()
	{
		return OauthClient::class;
	}

    public function validator()
    {
        return OauthClientValidator::class;
    }

    public function presenter()
    {
        return OauthClientPresenter::class;
    }
}
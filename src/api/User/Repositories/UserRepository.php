<?php

namespace Api\User\Repositories;

use Api\User\Interfaces\IUserRepository;
use Api\User\Models\User;
use Api\User\Presenters\UserPresenter;
use Api\User\Validators\UserValidator;
use Infrastructure\Database\Eloquent\Repository;


class UserRepository extends Repository implements IUserRepository
{

    protected $fieldSearchable = [
        'id',
        'social_reason'=>'like',
        'email' => 'like',
    ];

    public function model()
    {
        return User::class;
    }

    public function validator()
    {
        return UserValidator::class;
    }

    public function presenter()
    {
        return UserPresenter::class;
    }
}

<?php
use Illuminate\Support\Facades\Route;

Route::post('users', 'UserController@store')->name('users.store');

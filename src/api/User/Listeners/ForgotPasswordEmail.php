<?php

namespace Api\User\Listeners;

use Api\User\Mails\ForgotPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Infrastructure\Services\Mail\IMailService;


class ForgotPasswordEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    private $mailService;

    function __construct(IMailService $mailService)
    {
        $this->mailService = $mailService;
    }

    public function handle($event)
    {
        $user = $event->user;

        return ($this->mailService::createBuilder()
            ->setUser($user)
            ->setSubject("Redefinição de Senha")
            ->setMailableTemplate(new ForgotPassword($user, $event->token))
            ->setTo([$user->email => $user->name])
            ->setFrom([config('services.email.reset_password.from')])
            ->build())
            ->send();
    }
}

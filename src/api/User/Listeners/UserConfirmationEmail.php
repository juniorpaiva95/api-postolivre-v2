<?php
/**
 * Created by IntelliJ IDEA.
 * User: stalo
 * Date: 15/02/18
 * Time: 16:32
 */

namespace Api\User\Listeners;

use Api\User\Mails\UserConfirmation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Infrastructure\Services\Mail\IMailService;

class UserConfirmationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    private $mailService;

    public function __construct(IMailService $mailService)
    {
        $this->mailService = $mailService;
    }

    public function handle($event)
	{
        $user = $event->user;

        $status = ($this->mailService::createBuilder()

            ->setUser($user)
            ->setSubject("Confirmação de Cadastro")
            ->setMailableTemplate(new UserConfirmation($user))
            ->setTo([$user->email => $user->social_reason])
            ->setFrom([env('MAIL_FROM_ADDRESS') => "Posto Livre"])
            ->build())

//            ->setUser($event->user)
//            ->setSubject("Confirmação de Cadastro")
//            ->setMailableTemplate(new UserConfirmation($user))
//            ->setTo([$user->email => $user->name])
//            ->setFrom([config('services.email.confirmation.from')])
//            ->build())
            ->send();

        return $status;
	}
}

<?php

namespace Api\User;

use Api\User\Events\OauthClientWasCreated;
use Api\User\Events\OauthClientWasDeleted;
use Api\User\Events\PermissionWasCreated;
use Api\User\Events\PermissionWasDeleted;
use Api\User\Events\PermissionWasUpdated;
use Api\User\Events\RoleWasCreated;
use Api\User\Events\RoleWasDeleted;
use Api\User\Events\RoleWasUpdated;
use Api\User\Events\UserForgotPassword;
use Api\User\Events\UserWasDeleted;
use Api\User\Events\UserWasNotConfirmed;
use Api\User\Events\UserWasUpdated;
use Api\User\Events\UserWasCreated;
use Api\User\Interfaces\IActivityLogRepository;
use Api\User\Interfaces\IActivityLogService;
use Api\User\Interfaces\IAddressRepository;
use Api\User\Interfaces\IBankAccountRepository;
use Api\User\Interfaces\IDistributorRepository;
use Api\User\Interfaces\IOauthAccessTokenRepository;
use Api\User\Interfaces\IOauthAccessTokenService;
use Api\User\Interfaces\IOauthClientRepository;
use Api\User\Interfaces\IOauthClientService;
use Api\User\Interfaces\IPermissionRepository;
use Api\User\Interfaces\IPermissionService;
use Api\User\Interfaces\IRoleRepository;
use Api\User\Interfaces\IRoleService;
use Api\User\Interfaces\IUserRepository;
use Api\User\Interfaces\IUserService;
use Api\User\Listeners\ForgotPasswordEmail;
use Api\User\Listeners\UserConfirmationEmail;
use Api\User\Repositories\ActivityLogRepository;
use Api\User\Repositories\AddressRepository;
use Api\User\Repositories\BankAccountRepository;
use Api\User\Repositories\DistributorRepository;
use Api\User\Repositories\OauthAccessTokenRepository;
use Api\User\Repositories\OauthClientRepository;
use Api\User\Repositories\PermissionRepository;
use Api\User\Repositories\RoleRepository;
use Api\User\Repositories\UserRepository;
use Api\User\Services\ActivityLogService;
use Api\User\Services\OauthAccessTokenService;
use Api\User\Services\OauthClientService;
use Api\User\Services\PermissionService;
use Api\User\Services\RoleService;
use Api\User\Services\UserService;
use Infrastructure\Providers\EventServiceProvider;
use Infrastructure\Listeners\LogUserActivity;


class UserServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UserWasCreated::class => [
            UserConfirmationEmail::class,
            LogUserActivity::class
        ],
        UserWasDeleted::class => [
            LogUserActivity::class
        ],
        UserWasUpdated::class => [
            LogUserActivity::class
        ],
        UserForgotPassword::class => [
            ForgotPasswordEmail::class,
            LogUserActivity::class,
        ],
        UserWasNotConfirmed::class => [
            UserConfirmationEmail::class
        ],
        RoleWasCreated::class => [
            LogUserActivity::class
        ],
        RoleWasDeleted::class => [
            LogUserActivity::class
        ],
        RoleWasUpdated::class => [
            LogUserActivity::class
        ],
        PermissionWasCreated::class => [
            LogUserActivity::class
        ],
        PermissionWasDeleted::class => [
            LogUserActivity::class
        ],
        PermissionWasUpdated::class => [
            LogUserActivity::class
        ],
        OauthClientWasCreated::class => [
            LogUserActivity::class
        ],
        OauthClientWasDeleted::class => [
            LogUserActivity::class
        ],
    ];

    public function register()
    {
        $this->app->bind(IUserService::class, UserService::class);
        $this->app->bind(IUserRepository::class, UserRepository::class);

        $this->app->bind(IActivityLogService::class, ActivityLogService::class);
        $this->app->bind(IActivityLogRepository::class, ActivityLogRepository::class);

        $this->app->bind(IOauthClientService::class, OauthClientService::class);
        $this->app->bind(IOauthClientRepository::class, OauthClientRepository::class);

        $this->app->bind(IOauthAccessTokenService::class, OauthAccessTokenService::class);
        $this->app->bind(IOauthAccessTokenRepository::class, OauthAccessTokenRepository::class);

        $this->app->bind(IRoleService::class, RoleService::class);
        $this->app->bind(IRoleRepository::class, RoleRepository::class);

        $this->app->bind(IPermissionService::class, PermissionService::class);
        $this->app->bind(IPermissionRepository::class, PermissionRepository::class);

        $this->app->bind(IAddressRepository::class, AddressRepository::class);

        $this->app->bind(IBankAccountRepository::class, BankAccountRepository::class);

        $this->app->bind(IDistributorRepository::class, DistributorRepository::class);
    }
}

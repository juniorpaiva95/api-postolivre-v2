<?php

namespace Api\User\Exceptions;

use Exception;
use Throwable;

class NotConfirmedUserException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct(trans('exceptions.not_confirmed_user'), 401, $previous);
    }
}
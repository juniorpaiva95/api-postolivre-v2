<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 24/04/18
 * Time: 16:43
 */

namespace Api\User\Channels;

class NotificationChannel
{
    public function join($user_id, $id)
    {
        return (int) $user_id === (int) $id;
    }
}
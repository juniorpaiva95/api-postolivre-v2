<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 27/03/18
 * Time: 11:19
 */

namespace Api\User\Models;

use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Spatie\Permission\Models\Permission as BasePermission;

class Permission extends BasePermission
{
    protected $fillable = [ 'name', 'label'];

    protected $hidden = [ 'api' ];

    public function users(): MorphToMany
    {
        return $this->morphedByMany(
            User::class,
            'model',
            config('permission.table_names.model_has_permissions'),
            'permission_id',
            'model_id'
        );
    }
}
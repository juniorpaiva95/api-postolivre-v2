<?php

namespace Api\User\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Infrastructure\Traits\Uuids;
use Spatie\Activitylog\Models\Activity as BaseActivity;

class Activity extends BaseActivity
{
    use SoftDeletes, Uuids;

    protected $table = 'activity_log';
    public $incrementing = false;

    protected $casts = [
        'properties' => 'collection',
    ];
}

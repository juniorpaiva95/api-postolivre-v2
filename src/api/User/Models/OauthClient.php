<?php
/**
 * Created by IntelliJ IDEA.
 * User: stalo
 * Date: 10/01/18
 * Time: 11:31
 */

namespace Api\User\Models;

use Api\Client\Models\Contact;
use Carbon\Carbon;
use Infrastructure\Database\Eloquent\Model;

class OauthClient extends Model {

	protected $table = 'oauth_clients';
	
	protected $fillable = ['name', 'user_id', 'contact_id', 'secret', 'redirect', 'revoked'];
	protected $attributes = [
		'personal_access_client' => 0,
		'password_client' => 0,
		'revoked' => 0
	];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    public function oauthAccessTokens()
    {
        return $this->hasMany(OauthAccessToken::class, 'client_id')
            ->where('revoked', '=',false)
            ->where('expires_at', '>=', Carbon::now('UTC'))
            ->latest();
    }
}
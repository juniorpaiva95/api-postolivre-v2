<?php

namespace Api\User\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Traits\Uuids;

class Address extends Model
{
    use Uuids, SoftDeletes;
    protected $table = 'address';

    public $incrementing = false;

    protected $keyType = 'uuid';

    protected $fillable = [
        'street',
        'city',
        'state',
        'number',
        'complement',
        'cep',
        'neighborhood'
    ];
}

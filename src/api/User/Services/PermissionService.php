<?php

namespace Api\User\Services;

use Api\User\Events\PermissionWasCreated;
use Api\User\Events\PermissionWasDeleted;
use Api\User\Events\PermissionWasUpdated;
use Api\User\Interfaces\IPermissionRepository;
use Api\User\Interfaces\IPermissionService;
use Infrastructure\Services\Service;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;

use Spatie\Permission\Traits;

class PermissionService extends Service implements IPermissionService
{

    public function __construct(DatabaseManager $database,
                                Dispatcher $dispatcher,
                                IPermissionRepository $repository)
    {

        parent::__construct($database, $dispatcher, $repository);

        $this->modelCreated = PermissionWasCreated::class;
        $this->modelDeleted = PermissionWasDeleted::class;
        $this->modelUpdated = PermissionWasUpdated::class;
    }
}

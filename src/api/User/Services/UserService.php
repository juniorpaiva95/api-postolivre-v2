<?php

namespace Api\User\Services;

use Api\Station\Interfaces\IStationRepository;
use Api\Station\Interfaces\IStationService;
use Api\Station\Models\Station;
use Api\Unit\Interfaces\IUnitRepository;
use Api\Unit\Models\Unit;
use Api\Upload\Interfaces\IUploadService;
use Api\User\Events\UserForgotPassword;
use Api\User\Events\UserWasCreated;
use Api\User\Events\UserWasDeleted;
use Api\User\Events\UserWasNotConfirmed;
use Api\User\Events\UserWasUpdated;

use Api\User\Exceptions\NotConfirmedUserException;
use Api\User\Interfaces\IAddressRepository;
use Api\User\Interfaces\IBankAccountRepository;
use Api\User\Interfaces\IDistributorRepository;
use Api\User\Interfaces\IRoleRepository;
use Api\User\Interfaces\IUserRepository;
use Api\User\Interfaces\IUserService;
use Api\User\Models\Distributor;
use Api\User\Models\User;
use Api\User\Validators\UserValidator;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Enums\RoleTypeEnum;
use Infrastructure\Enums\UploadTypeEnum;
use Infrastructure\Services\Service;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;

use Illuminate\Contracts\Auth\PasswordBroker as IPasswordBroker;
use Spatie\Permission\Exceptions\RoleDoesNotExist;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class UserService extends Service implements IUserService
{
    const STATION = 'station';
    const DISTRIBUTOR = 'distributor';

    private $passwordBroker;
    private $addressRepository;
    private $bankAccountRepository;
    private $distributorRepository;
    private $stationRepository;
    private $stationService;
    private $uploadService;
    private $unitRepository;
    private $roleRepository;

    public function __construct(DatabaseManager $database,
                                Dispatcher $dispatcher,
                                IUserRepository $repository,
                                IAddressRepository $addressRepository,
                                IDistributorRepository $distributorRepository,
                                IStationRepository $stationRepository,
                                IStationService $stationService,
                                IUnitRepository $unitRepository,
                                IRoleRepository $roleRepository,
                                IUploadService $uploadService,
                                IBankAccountRepository $bankAccountRepository,
                                IPasswordBroker $passwordBroker)
    {
        parent::__construct($database, $dispatcher, $repository);

        $this->roleRepository = $roleRepository;
        $this->unitRepository = $unitRepository;
        $this->uploadService = $uploadService;
        $this->bankAccountRepository = $bankAccountRepository;
        $this->stationService = $stationService;
        $this->distributorRepository = $distributorRepository;
        $this->addressRepository = $addressRepository;

        $this->passwordBroker = $passwordBroker;
        $this->modelCreated = UserWasCreated::class;
        $this->modelUpdated = UserWasUpdated::class;
        $this->modelDeleted = UserWasDeleted::class;
        $this->autoIncrement = true;
    }


    /**
     * Método que cria o usuário e faz o upload das imagens
     * @param array $data
     */
    private function createStationUser(array $data) : void
    {
        $userStation = $this->stationService->create($data);
        $this->uploadStationImages($data, $userStation);
    }

    /**
     * Método que faz o upload de imagens e associa ao Posto
     * @param array $data
     * @param Station $userStation
     */
    private function uploadStationImages(array $data, Station $userStation): void {
        #Override type
        $data['type'] = UploadTypeEnum::STATION_PROFILE_PICTURE;
        $this->uploadService->create($data, $userStation, 'uploads');
    }

    //TODO: refact using $this->runService
    //@TODO: Refact this method for the saving address
    public function create(array $data, Model $model = NULL, $relation = NULL)
    {
        $user = null;
        $this->database->beginTransaction();

        try{

            // Forçando a validaçao do request antes do fluxo iniciar
            validateWithData($data,(new UserValidator(app('validator')))->getRules(UserValidator::RULE_CREATE));

            $unit = $this->unitRepository
                ->findWhere(['state_abbreviation' => $data['address']['state']])
                ->first();

            if (!$unit) {
                throw (new ModelNotFoundException())->setModel(Unit::class, $data['address']['state']);
            }

            $data['unit_id'] = $unit->id;
            $data['password'] = $data['password'] ?? Hash::make(Str::random(40));
            $data['identifier'] = $this->repository->increment(['id']);
            $user = $this->repository->create($data);
            $address = $this->addressRepository->create($data['address']);

            //Update user after create address
            $this->repository->update(['address_id'=> $address->id], $user->id);

            if ($data['type'] === self::DISTRIBUTOR) {
                // Relation Bank Account
                $role = $this->roleRepository->findWhere(['name' => RoleTypeEnum::DISTRIBUTOR])->first();
                $bankAccount = $this->bankAccountRepository->create($data['bank']);
                $this->distributorRepository->create([
                    'user_id' => $user->id,
                    'bank_account_id' => $bankAccount->id
                ]);
            } elseif ($data['type'] === self::STATION) {
                $role = $this->roleRepository->findWhere(['name' => RoleTypeEnum::STATION])->first();
                $this->createStationUser(array_merge($data, ['user_id'=> $user->id]));
            } elseif($data['type'] === RoleTypeEnum::ADMIN) {
                $role = $this->roleRepository->findWhere(['name' => RoleTypeEnum::ADMIN])->first();
            }

            if (!$role) {
                throw new Exception("Nenhum perfil encontrado para ${$data['type']}");
            }

            $user->assignRole($role);
            $user['token'] = $this->passwordBroker->createToken($user);

            $this->dispatcher->dispatch(
                new $this->modelCreated($this->logActivity($user, entity($this->modelCreated))));

        }catch(RoleDoesNotExist $rex){
            $this->database->rollBack();
            throw $rex;
        }
        catch(Exception $ex){
            $this->database->rollBack();
            throw $ex;
        }

        $this->database->commit();
        return $this->repository->with(['address'])->find($user->id);
    }

    //TODO: refact using $this->runService
    public function update(array $data, $id)
	{
        /**
         * @var $user User
         * @var $distributor Distributor
         */
        $user = null;
        $this->database->beginTransaction();

        try{
            if(isset($data['password']))
                $data['password'] = Hash::make($data['password']);

            $user = parent::update($data, $id);
            $roles_id = $data['roles_id'] ?? null;

            if(!is_null($roles_id))
                $user->syncRoles($roles_id);

            if (isset($data['type']) && $data['type'] === self::DISTRIBUTOR) {
                if (isset($data['bank'])) {
                    $distributor = $user->distributor()->first();
                    $distributor->bankAccount()->update($data['bank']);
                }
            } else if (isset($data['type']) && $data['type'] === self::STATION) {
                # Delete all uploads associated
                $user->station->uploads()->delete();
                $this->uploadStationImages($data, $user->station);
            }

            if (isset($data['address'])) {
                $user->address()->update($data['address']);
            }
        } catch(RoleDoesNotExist $ex){
            $this->database->rollBack();
            throw $ex;
        } catch(Exception $ex){
            $this->database->rollBack();
            throw $ex;
        }

        $this->database->commit();

		return $user;
	}

    //TODO: refact using $this->runService
    public function sendResetLinkEmail($email)
    {
        $user = null;
        $this->database->beginTransaction();

        try{
            $user = $this->findWhere([['email', '=', $email]])->first() ?? null;

            if(!is_null($user)) {
                if(!$user->confirmed){;
                    $this->resendConfirmationEmail($user);
                }else {
                    $token = $this->passwordBroker->createToken($user);
                    //in this context, the return is a user
                    $userLog = $this->logActivity($user, __FUNCTION__);
                    $this->dispatcher->dispatch(new UserForgotPassword($userLog, $token));
                }
            }else{
                throw new NotFoundHttpException();
            }

        }catch(Exception $ex){
            $this->database->rollBack();
            throw $ex;
        }

        $this->database->commit();

        return $user;
    }

    public function resendConfirmationEmail($user){
        $user = $this->regenerateConfirmationToken($user);
        $this->dispatcher->dispatch(new UserWasNotConfirmed($user));

        throw new NotConfirmedUserException();
    }


    public function getRoles($id){

        $user = $this->repository->find($id);
        return $user->roles;
    }

    public function getUploads($id){

        $user = $this->repository->skipCache(true)->find($id);
        return $user->uploads;
    }

    public function getUploadByType($id, $type){

        $user = $this->repository->skipCache(true)->find($id);

        return collect($user->uploads)->filter(function($value) use ($type){
            return $value->type === $type;
        })->first();
    }

    public function getUploadedByMe($id){

        $user = $this->repository->find($id);
        return $user->uploadedByMe;
    }

    public function getPermissions($id)
    {
        $user = $this->repository->find($id);
        return $user->permissions;
    }

    public function getAllPermissions($id)
    {
        $user = $this->repository->find($id);
        return $user->getAllPermissions();
    }


    public function syncRoles($id, $modelIds){
        $this->syncOperation($id, $modelIds, __FUNCTION__);
    }

    public function syncPermissions($id, $modelIds){
        $this->syncOperation($id, $modelIds, __FUNCTION__);
    }


    private function regenerateConfirmationToken($user){
        $this->passwordBroker->deleteToken($user);
        $user['token'] = $this->passwordBroker->createToken($user);

        return $user;
    }

    private function syncOperation($id, $modelIds, $function){
        $user = $this->repository->find($id);
        $user->{$function}(array_unique($modelIds));
    }

}

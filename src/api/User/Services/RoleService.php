<?php

namespace Api\User\Services;

use Api\User\Events\RoleWasCreated;
use Api\User\Events\RoleWasDeleted;
use Api\User\Events\RoleWasUpdated;
use Api\User\Interfaces\IRoleRepository;
use Api\User\Interfaces\IRoleService;

use Infrastructure\Services\Service;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;


class RoleService extends Service implements IRoleService
{
    public function __construct(DatabaseManager $database,
                                Dispatcher $dispatcher,
                                IRoleRepository $repository)
    {
        parent::__construct($database, $dispatcher, $repository);

        $this->modelCreated = RoleWasCreated::class;
        $this->modelDeleted = RoleWasDeleted::class;
        $this->modelUpdated = RoleWasUpdated::class;


    }
}

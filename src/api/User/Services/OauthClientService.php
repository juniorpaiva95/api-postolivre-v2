<?php
/**
 * Created by IntelliJ IDEA.
 * User: stalo
 * Date: 10/01/18
 * Time: 11:53
 */

namespace Api\User\Services;

use Api\User\Events\OauthClientWasCreated;
use Api\User\Events\OauthClientWasDeleted;
use Api\User\Interfaces\IOauthClientRepository;
use Api\User\Interfaces\IOauthClientService;
use Infrastructure\Services\Service;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;

class OauthClientService extends Service implements IOauthClientService
{

    public function __construct(DatabaseManager $database,
                                Dispatcher $dispatcher,
                                IOauthClientRepository $repository)
    {
        parent::__construct($database, $dispatcher, $repository);

        $this->modelCreated = OauthClientWasCreated::class;
        $this->modelDeleted = OauthClientWasDeleted::class;
    }
}
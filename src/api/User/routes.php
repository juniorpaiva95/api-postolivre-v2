<?php
use Illuminate\Support\Facades\Route;

Route::apiResource('users', 'UserController')->except('store');
Route::put('users/{uuid}/restore', 'UserController@restore');

Route::apiResource('users.roles', 'UserRoleController')->only(['index', 'store'])
    ->middleware(['role:admin|manager']);

Route::apiResource('users.permissions', 'UserPermissionController')->only(['index', 'store'])
    ->middleware(['role:admin|manager']);

Route::apiResource('roles', 'RoleController');
//    ->middleware(['role:admin|manager']);

Route::apiResource('permissions', 'PermissionController')
    ->middleware(['role:admin|manager']);

Route::apiResource('activity-logs', 'ActivityLogController')
    ->middleware(['role:admin|manager']);

Route::apiResource('oauth-clients', 'OauthClientController')
    ->middleware(['role:admin']);

//$router->get('users/{uuid}/uploads/{type?}', 'UserUploadController@index');
//$router->post('users/{uuid}/uploads/{type?}', 'UserUploadController@store');

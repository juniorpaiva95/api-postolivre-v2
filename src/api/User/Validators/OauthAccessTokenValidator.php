<?php
namespace Api\User\Validators;

use Prettus\Validator\LaravelValidator;

class OauthAccessTokenValidator extends LaravelValidator
{
    protected $rules = [
        'name' => 'required',
    ];

    protected $attributes = [
        'name' => 'the client\'s name was not found'
    ];
}
<?php
namespace Api\User\Validators;

use Prettus\Validator\LaravelValidator;

class OauthClientValidator extends LaravelValidator
{
    protected $rules = [
        'name' => 'required',
        //'user_id' => 'required'
    ];

    protected $attributes = [
        'name' => 'the client\'s name was not found'
    ];
}
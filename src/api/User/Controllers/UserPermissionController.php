<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 14/04/18
 * Time: 16:06
 */

namespace Api\User\Controllers;

use Api\User\Interfaces\ITeamService;
use Api\User\Interfaces\IPermissionService;
use Api\User\Interfaces\IRoleService;
use Api\User\Interfaces\IUserService;
use Illuminate\Http\Request;
use Infrastructure\Http\Controller;

class UserPermissionController extends Controller
{
    protected $service;
    private $permissionService;

    public function __construct(IUserService $service, IPermissionService $permissionService){
        $this->service = $service;
        $this->permissionService = $permissionService;
    }

    public function index($id)
    {
        return $this->response(
            $this->permissionService->parserResult(
                $this->service->getAllPermissions($id)
            )
        );
    }

    public function store($id, Request $request)
    {
        validate($request,['permissions_id' => 'array']);

        $models_id = $request->get('permissions_id');
        $this->service->syncPermissions($id, $models_id);

        return $this->response(
            $this->permissionService->parserResult(
                $this->service->getAllPermissions($id)
            )
        );
    }

}

<?php

namespace Api\User\Controllers;

use Api\User\Interfaces\IOauthClientService;
use Api\User\Models\OauthClient;
use Infrastructure\Http\CrudController;

class OauthClientController extends CrudController
{
    public function __construct(IOauthClientService $service){
        parent::__construct($service);
    }

}

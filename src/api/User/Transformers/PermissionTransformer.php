<?php

namespace Api\User\Transformers;

use Api\User\Models\Permission;
use Api\User\Models\Role;
use Api\User\Models\User;
use League\Fractal\TransformerAbstract;

class PermissionTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['roles', 'users'];

    public function transform(Permission $model)
    {
        return [
            'id'      => (int) $model->id,
            'name'    => $model->name,
            'label'   => $model->label
        ];
    }

    public function includeRoles(Permission $model)
    {
        return $this->collection($model->roles, new RoleTransformer(), resourceKey(Role::class));
    }

    public function includeUsers(Permission $model)
    {
        return $this->collection($model->users, new UserTransformer(), resourceKey(User::class));
    }
}
<?php

namespace Api\User\Transformers;

use Api\User\Models\OauthAccessToken;
use League\Fractal\TransformerAbstract;

class OauthAccessTokenTransformer extends TransformerAbstract
{

    public function transform(OauthAccessToken $model)
    {
        return [
            'id'          => $model->id,
            'user_id'     => $model->user_id,
            'oauth_contact_id'  => $model->oauth_contact_id,
            'name'        => $model->name,
            'scopes'      => $model->scopes,
            'revoked'     => $model->revoked,
            'expires_at'  => $model->expires_at
        ];
    }
}
<?php

namespace Api\User\Transformers;

use Api\User\Models\BankAccount;
use League\Fractal\TransformerAbstract;

class BankAccountTransformer extends TransformerAbstract
{
    protected $availableIncludes = [

    ];

    public function transform(BankAccount $model)
    {
        return [
            'id'                => $model->id,
            'code'  => $model->code,
            'agency'=> $model->agency,
            'account'=> $model->account,
            'type'  => $model->type,
            'created_at' => $model->getCreatedAtColumn(),
            'updated_at' => $model->getUpdatedAtColumn(),
            'deleted_at' => $model->getDeletedAtColumn()
        ];
    }

}

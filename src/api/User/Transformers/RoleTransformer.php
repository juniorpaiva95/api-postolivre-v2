<?php

namespace Api\User\Transformers;

use Api\User\Models\Permission;
use Api\User\Models\Role;
use Api\User\Models\User;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['permissions', 'users'];

    public function transform(Role $model)
    {
        return [
            'id'            => $model->id,
            'name'          => $model->name,
            'created_at'    => $model->created_at,
            'updated_at'    => $model->updated_at
        ];
    }

    public function includePermissions(Role $model)
    {
        return $this->collection($model->getPermissions(), new PermissionTransformer(), resourceKey(Permission::class));
    }

    public function includeUsers(Role $model)
    {
        return $this->collection($model->users, new UserTransformer(), resourceKey(User::class));
    }
}

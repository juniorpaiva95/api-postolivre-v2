<?php

namespace Api\User\Transformers;

use Api\User\Models\Address;
use Api\User\Models\BankAccount;
use Api\User\Models\Distributor;
use League\Fractal\TransformerAbstract;

class DistributorTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'bank_account'
    ];

    public function transform(Distributor $model)
    {
        return [
            'id' => $model->id,
            'bank_account_id' => $model->bank_account_id,
            'user_id' => $model->user_id,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
            'deleted_at' => $model->deleted_at
        ];
    }

    public function includeBankAccount(Distributor $model)
    {
        if ($model->bankAccount === null) {
            return $this->null();
        }

        return $this->item($model->bankAccount, new BankAccountTransformer(), resourceKey(BankAccount::class));
    }

}

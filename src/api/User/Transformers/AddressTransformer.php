<?php

namespace Api\User\Transformers;

use Api\User\Models\Address;
use League\Fractal\TransformerAbstract;

class AddressTransformer extends TransformerAbstract
{
    protected $availableIncludes = [

    ];

    public function transform(Address $model)
    {
        return [
            'id'                => $model->id,
            'street'             => $model->street,
            'city'     => $model->city,
            'state'=> $model->state,
            'number'      => $model->number,
            'complement'            => $model->complement,
            'cep'      => $model->cep,
            'neighborhood'  => $model->neighborhood
        ];
    }

}

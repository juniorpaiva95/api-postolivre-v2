<?php

namespace Api\User\Transformers;

use Api\User\Models\OauthAccessToken;
use Api\User\Models\OauthClient;
use League\Fractal\TransformerAbstract;

class OauthClientTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['oauthAccessTokens'];

    public function transform(OauthClient $model)
    {
        return [
            'id'          => $model->id,
            'user_id'     => $model->user_id,
            'name'        => $model->name,
            'secret'      => $model->secret,
            'revoked'     => $model->revoked
        ];
    }

    public function includeOauthAccessTokens(OauthClient $model)
    {
        if ($model->oauthAccessTokens === null) {
            return $this->null();
        }

        return $this->collection($model->oauthAccessTokens, new OauthAccessTokenTransformer(), resourceKey(OauthAccessToken::class));
    }
}
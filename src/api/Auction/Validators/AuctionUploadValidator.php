<?php


namespace Api\Auction\Validators;


use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class AuctionUploadValidator extends LaravelValidator
{
    public static $arrayRules = [
        'file' => 'required|file'
    ];
//    protected $rules = [
//        ValidatorInterface::RULE_CREATE => [
//            'file' => 'required|file'
//        ],
//        ValidatorInterface::RULE_UPDATE => [
//            'title' => ['string'],
//        ]
//    ];

}

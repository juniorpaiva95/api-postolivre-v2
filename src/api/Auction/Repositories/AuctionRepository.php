<?php

namespace Api\Auction\Repositories;

use Api\Auction\Interfaces\IAuctionRepository;
use Api\Auction\Models\Auction;
use Api\Auction\Presenters\AuctionPresenter;
use Api\Auction\Validators\AuctionValidator;
use Infrastructure\Database\Eloquent\Repository;


class AuctionRepository extends Repository implements IAuctionRepository
{

    protected $fieldSearchable = [
        'id',
        'name'=> 'like',
        'freight_type',
        'fuel.name' => 'like',
        'cpf_or_cnpj' => 'like',
        'station_id',
        'fuel_id'
    ];

    public function model()
    {
        return Auction::class;
    }

    public function validator()
    {
        return AuctionValidator::class;
    }

    public function presenter()
    {
        return AuctionPresenter::class;
    }
}

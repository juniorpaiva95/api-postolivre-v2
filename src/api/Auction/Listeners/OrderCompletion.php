<?php

namespace Api\Auction\Listeners;

use Api\Auction\Models\Auction;
use Api\Lot\Interfaces\ILotRepository;
use Api\Lot\Models\Lot;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class OrderCompletion
{
    private $lotRepository;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ILotRepository $lotRepository)
    {
        $this->lotRepository = $lotRepository;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        /**
         * @var $model Auction
         * @var $lot Lot
         */
        $model = $event->model;
        $lot = $model->lot;

        if ($lot) {
            $isFinish = collect($lot->auctions)
                    ->filter(function($item){
                        return $item->payment_validated_at !== null;
                    })->count() > 0;

            if ($isFinish) {
                //Notificar todos os postos que o pedido foi finalizado
                collect($lot->auctions)->each(function (Auction $auction) {
                    Notification::send($auction->station->user, new \App\Notifications\OrderCompletion());
                });

            }
        }

    }
}

<?php

namespace Api\Auction\Events;

use Infrastructure\Events\Event;
use Api\Auction\Models\Auction;

class AuctionWasDeleted extends Event
{
    public $unit;

    public function __construct(Auction $unit)
    {
        $this->unit = $unit;
    }
}

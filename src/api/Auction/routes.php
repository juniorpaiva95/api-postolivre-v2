<?php
use Illuminate\Support\Facades\Route;
$router->apiResource('auctions', 'AuctionController');

Route::post('auctions/{uuid}/upload', 'AuctionUploadController@uploadPaymentVoucher');

<?php

namespace Api\Auction\Controllers;

use Api\Auction\Interfaces\IAuctionService;
use Illuminate\Http\Request;
use Infrastructure\Http\CrudController;

/**
 * Class FuelController
 * @package Api\Auction\Controllers
 */
class AuctionController extends CrudController
{
    /**
     * FuelController constructor.
     * @param IAuctionService $service
     */
    public function __construct(IAuctionService $service)
    {
        parent::__construct($service);
    }
}

<?php

namespace Api\Station\Repositories;

use Api\Station\Interfaces\IStationRepository;
use Api\Station\Models\Station;
use Api\Station\Presenters\StationPresenter;
use Api\Station\Validators\StationValidator;
use Infrastructure\Database\Eloquent\Repository;


class StationRepository extends Repository implements IStationRepository
{

//    protected $fieldSearchable = [
//        'id',
//        'name'=>'like',
//        'cpf_or_cnpj' => 'like',
//    ];

    public function model()
    {
        return Station::class;
    }

    public function validator()
    {
        return StationValidator::class;
    }

    public function presenter()
    {
        return StationPresenter::class;
    }
}

<?php

namespace Api\Station\Events;

use Infrastructure\Events\Event;
use Api\Station\Models\Station;

class StationWasCreated extends Event
{
    public $unit;

    public function __construct(Station $unit)
    {
        $this->unit = $unit;
    }
}

<?php

namespace Api\Station\Services;

use Api\Station\Events\StationWasCreated;
use Api\Station\Events\StationWasDeleted;
use Api\Station\Events\StationWasUpdated;
use Api\Station\Interfaces\IStationRepository;
use Api\Station\Interfaces\IStationService;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Infrastructure\Services\Service;

class StationService extends Service implements IStationService
{
    public function __construct(DatabaseManager $database, Dispatcher $dispatcher, IStationRepository $repository)
    {
        parent::__construct($database, $dispatcher, $repository);

        $this->modelCreated = StationWasCreated::class;
        $this->modelUpdated = StationWasUpdated::class;
        $this->modelDeleted = StationWasDeleted::class;
        $this->autoIncrement = true;
    }
}

<?php

namespace Api\Station\Models;

use Api\Auction\Models\Auction;
use Api\Upload\Models\Upload;
use Api\User\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Enums\UploadTypeEnum;
use Infrastructure\Traits\Uuids;

class Station extends Model
{
    use Uuids, SoftDeletes;

    protected $table = 'stations';

    protected $keyType = 'uuid';

    public $incrementing = false;

    protected $fillable = [
        'user_id',
        'identifier'
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];


    /**
     * Relationships
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function auctions()
    {
        return $this->hasMany(Auction::class, 'station_id');
    }

    /**
     * Get all of the uploads attached to this station.
     */
    public function uploads()
    {
        return $this->morphMany(Upload::class, 'uploadable')
            ->where('type', UploadTypeEnum::STATION_PROFILE_PICTURE);
    }
}

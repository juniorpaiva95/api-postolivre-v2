<?php

namespace Api\Station\Interfaces;

use Infrastructure\Database\IRepository;

interface IStationRepository extends IRepository
{

}

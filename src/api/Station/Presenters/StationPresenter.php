<?php

namespace Api\Station\Presenters;

use Api\Station\Transformer\StationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class StationPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'stations';
    protected $resourceKeyCollection = 'stations';

    public function getTransformer()
    {
        return new StationTransformer();
    }
}

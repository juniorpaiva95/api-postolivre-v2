<?php

namespace Api\Station;

use Api\Station\Events\StationWasCreated;
use Api\Station\Events\StationWasDeleted;
use Api\Station\Events\StationWasUpdated;
use Api\Station\Interfaces\IStationRepository;
use Api\Station\Interfaces\IStationService;
use Api\Station\Repositories\StationRepository;
use Api\Station\Services\StationService;
use Infrastructure\Listeners\LogUserActivity;
use Infrastructure\Providers\EventServiceProvider;

class StationServiceProvider extends EventServiceProvider
{
    protected $listen = [
        StationWasCreated::class => [
            LogUserActivity::class
        ],
        StationWasDeleted::class => [
            LogUserActivity::class
        ],
        StationWasUpdated::class => [
            LogUserActivity::class
        ],
    ];

    public function register()
    {
        $this->app->bind(IStationService::class, StationService::class);
        $this->app->bind(IStationRepository::class, StationRepository::class);
    }
}

<?php

namespace Api\Bid\Models;

use Api\Auction\Models\Auction;
use Api\Lot\Models\Lot;
use Api\User\Models\Distributor;
use Illuminate\Database\Eloquent\SoftDeletes;
use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Traits\Uuids;

class Bid extends Model
{
    use Uuids, SoftDeletes;

    public $incrementing = false;

    protected $table = 'bids';

    protected $keyType = 'uuid';

    protected $fillable = ['distributor_id', 'lot_id', 'value', 'identifier'];

    protected $casts = [
        'value' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];

    public function distributor() {
        return $this->belongsTo(Distributor::class);
    }

//    public function auction() {
//        return $this->belongsTo(Auction::class);
//    }

    public function lot() {
        return $this->belongsTo(Lot::class);
    }
}

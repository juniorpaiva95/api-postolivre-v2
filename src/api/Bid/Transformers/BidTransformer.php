<?php

namespace Api\Bid\Transformer;

use Api\Auction\Models\Auction;
use Api\Auction\Transformer\AuctionTransformer;
use Api\Bid\Models\Bid;
use Api\Lot\Models\Lot;
use Api\Lot\Transformers\LotTransformer;
use Api\User\Models\Distributor;
use Api\User\Transformers\DistributorTransformer;
use League\Fractal\TransformerAbstract;

class BidTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['distributor', 'lot'];

    public function transform(Bid $model)
    {
        return [
            'id'                => $model->id,
            'identifier'        => $model->identifier,
            'value'             => $model->value,
            'created_at'        => $model->created_at,
            'updated_at'        => $model->updated_at,
            'deleted_at'        => $model->deleted_at
        ];
    }

    public function includeDistributor(Bid $model) {
        if (null === $model->distributor) {
            return $this->null();
        }

        return $this->item($model->distributor, new DistributorTransformer(), resourceKey(Distributor::class));
    }

    public function includeAuction(Bid $model) {
        if (null === $model->auction) {
            return $this->null();
        }

        return $this->item($model->auction, new AuctionTransformer(), resourceKey(Auction::class));
    }

    public function includeLot(Bid $model) {
        if (null === $model->lot) {
            return $this->null();
        }

        return $this->item($model->lot, new LotTransformer(), resourceKey(Lot::class));
    }
}

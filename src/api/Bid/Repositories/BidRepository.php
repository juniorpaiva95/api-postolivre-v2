<?php

namespace Api\Bid\Repositories;

use Api\Bid\Interfaces\IBidRepository;
use Api\Bid\Models\Bid;
use Api\Bid\Presenters\BidPresenter;
use Api\Bid\Validators\BidValidator;
use Infrastructure\Database\Eloquent\Repository;


class BidRepository extends Repository implements IBidRepository
{

    protected $fieldSearchable = [
        'id',
        'value',
        'auction_id',
        'distributor_id'
    ];

    public function model()
    {
        return Bid::class;
    }

    public function validator()
    {
        return BidValidator::class;
    }

    public function presenter()
    {
        return BidPresenter::class;
    }
}

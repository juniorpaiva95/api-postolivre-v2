<?php

namespace Api\Bid\Controllers;

use Api\Bid\Interfaces\IBidService;
use Infrastructure\Http\CrudController;

/**
 * Class BidController
 * @package Api\Bid\Controllers
 */
class BidController extends CrudController
{
    /**
     * BidController constructor.
     * @param IBidService $service
     */
    public function __construct(IBidService $service)
    {
        parent::__construct($service);
    }
}

<?php

namespace Api\Port\Controllers;

use Api\Port\Interfaces\IPortService;
use Infrastructure\Http\CrudController;

/**
 * Class PortController
 * @package Api\Port\Controllers
 */
class PortController extends CrudController
{
    /**
     * PortController constructor.
     * @param IPortService $service
     */
    public function __construct(IPortService $service)
    {
        parent::__construct($service);
    }
}

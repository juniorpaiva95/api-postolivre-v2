<?php
namespace Api\Port\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class PortValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|string|unique:ports',
            'unit_id' => 'required|exists:units,id'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'string|unique:ports',
            'unit_id' => 'exists:units,id'
        ]
    ];

}

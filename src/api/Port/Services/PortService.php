<?php

namespace Api\Port\Services;

use Api\Port\Events\PortWasCreated;
use Api\Port\Events\PortWasDeleted;
use Api\Port\Events\PortWasUpdated;
use Api\Port\Interfaces\IPortRepository;
use Api\Port\Interfaces\IPortService;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Infrastructure\Services\Service;

class PortService extends Service implements IPortService
{
    public function __construct(DatabaseManager $database, Dispatcher $dispatcher, IPortRepository $repository)
    {
        parent::__construct($database, $dispatcher, $repository);

        $this->modelCreated = PortWasCreated::class;
        $this->modelUpdated = PortWasUpdated::class;
        $this->modelDeleted = PortWasDeleted::class;
        $this->autoIncrement = true;
    }
}

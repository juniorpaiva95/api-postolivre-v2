<?php

namespace Api\Port\Events;

use Infrastructure\Events\Event;
use Api\Port\Models\Port;

class PortWasCreated extends Event
{
    public $port;

    public function __construct(Port $port)
    {
        $this->port = $port;
    }
}

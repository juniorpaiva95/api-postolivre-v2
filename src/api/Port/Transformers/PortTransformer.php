<?php

namespace Api\Port\Transformers;

use Api\Port\Models\Port;
use Api\Unit\Models\Unit;
use Api\Unit\Transformer\UnitTransformer;
use Infrastructure\Enums\StateEnum;
use League\Fractal\TransformerAbstract;

class PortTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['unit'];

    public function transform(Port $model)
    {
//        dd($model);
        return [
            'id'                => $model->id,
            'identifier'        => $model->identifier,
            'name'              => $model->name,
//            'state_abbreviation'=> $model->unit->state_abbreviation,
//            'state_name'        => StateEnum::getEstadoBySigla($model->unit->state_abbreviation),
            'created_at'        => $model->created_at,
            'updated_at'        => $model->updated_at,
            'deleted_at'        => $model->deleted_at
        ];
    }

    public function includeUnit(Port $model)
    {
        if (null === $model->unit) {
            return $this->null();
        }

        return $this->item($model->unit, new UnitTransformer(), resourceKey(Unit::class));
    }
}

<?php


namespace Api\Port\Presenters;


use Api\Port\Transformers\PortTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class PortPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'port';
    protected $resourceKeyCollection = 'ports';

    public function getTransformer()
    {
        return new PortTransformer();
    }
}

<?php

namespace Api\Upload\Interfaces;

use Infrastructure\Database\IRepository;

interface IUploadRepository extends IRepository
{
    public function skipCache($status = true);
}
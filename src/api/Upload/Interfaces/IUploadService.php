<?php
/**
 * Created by PhpStorm.
 * User: leonardolobato
 * Date: 16/01/18
 * Time: 19:13
 */

namespace Api\Upload\Interfaces;

use Infrastructure\Database\Eloquent\Model;
use Infrastructure\Services\IService;

interface IUploadService extends IService
{
    public function update(array $data, $id, Model $model = null);
}
<?php

namespace Api\Upload\Events;

use Api\Upload\Models\Upload;
use Infrastructure\Events\Event;

class UploadWasUpdated extends Event
{
    public $upload;

    public function __construct(Upload $upload)
    {
        $this->upload = $upload;
    }
}

<?php

namespace Api\Upload\Listeners;

use Api\Upload\Models\Upload;
use Hyn\Tenancy\Traits\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use Infrastructure\Events\Event;
use Infrastructure\Services\Upload\Providers\IUploadFileService;

class ChangeHistoryUploadPrivacy extends Event
{
    use DispatchesJobs, InteractsWithQueue;

    private $uploadService;

    public function __construct(IUploadFileService $uploadService)
    {
        $this->uploadService = $uploadService;
    }

    /**
     * @param $event
     */
    public function handle($event)
    {
        $upload = $event->upload;
        $histories = $upload->uploadHistories;

        foreach($histories as $history){
            $attributes = json_decode($history->attributes, true);
            $attributes['is_public'] = $upload->is_public;
            $model = new Upload();
            $model->fill($attributes);

            $this->uploadService->move($model);
        }
    }
}
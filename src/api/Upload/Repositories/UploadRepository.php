<?php

namespace Api\Upload\Repositories;

use Api\Upload\Interfaces\IUploadRepository;
use Api\Upload\Models\Upload;
use Api\Upload\Presenters\UploadPresenter;
use Api\Upload\Validators\UploadValidator;
use Infrastructure\Database\Eloquent\Repository;


class UploadRepository extends Repository implements IUploadRepository
{
    protected $fieldSearchable = [
        'uuid' =>'like',
        'filename' =>'like',
        'url' =>'like',
        'mime' =>'like',
        'type',
        'title' =>'like',
        'description' =>'like'
    ];

    public function model()
    {
        return Upload::class;
    }

    public function validator()
    {
        return UploadValidator::class;
    }

    public function presenter()
    {
        return UploadPresenter::class;
    }

}

<?php
namespace Api\Upload\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class UploadValidator extends LaravelValidator
{
    protected $rules = [
    ];

    public static $arrayRules = [
        'title'         => 'string',
        'description'   => 'string',
        //'is_public'     => 'boolean',
        'type'          => 'string'
    ];
}

<?php

namespace Api\Upload\Presenters;

use Api\Upload\Transformers\UploadTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class UploadPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'uploads';
    protected $resourceKeyCollection = 'uploads';

    public function getTransformer()
    {
        return new UploadTransformer();
    }
}
<?php

namespace Api\Fuel\Services;

use Api\Fuel\Events\FuelWasCreated;
use Api\Fuel\Events\FuelWasDeleted;
use Api\Fuel\Events\FuelWasUpdated;
use Api\Fuel\Interfaces\IFuelRepository;
use Api\Fuel\Interfaces\IFuelService;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Infrastructure\Services\Service;

class FuelService extends Service implements IFuelService
{
    public function __construct(DatabaseManager $database, Dispatcher $dispatcher, IFuelRepository $repository)
    {
        parent::__construct($database, $dispatcher, $repository);

        $this->modelCreated = FuelWasCreated::class;
        $this->modelUpdated = FuelWasUpdated::class;
        $this->modelDeleted = FuelWasDeleted::class;
        $this->autoIncrement = true;
    }
}

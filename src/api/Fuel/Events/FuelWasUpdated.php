<?php

namespace Api\Fuel\Events;

use Infrastructure\Events\Event;
use Api\Fuel\Models\Fuel;

class FuelWasUpdated extends Event
{
    public $unit;

    public function __construct(Fuel $unit)
    {
        $this->unit = $unit;
    }
}

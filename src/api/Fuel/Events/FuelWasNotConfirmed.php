<?php

namespace Api\Fuel\Events;

use Api\Fuel\Models\Fuel;
use Infrastructure\Events\Event;

class FuelWasNotConfirmed extends Event
{
    public $unit;

    public function __construct(Fuel $unit)
    {
        $this->unit = $unit;
    }
}

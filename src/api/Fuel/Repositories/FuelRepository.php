<?php

namespace Api\Fuel\Repositories;

use Api\Fuel\Interfaces\IFuelRepository;
use Api\Fuel\Models\Fuel;
use Api\Fuel\Presenters\FuelPresenter;
use Api\Fuel\Validators\FuelValidator;
use Infrastructure\Database\Eloquent\Repository;


class FuelRepository extends Repository implements IFuelRepository
{

    protected $fieldSearchable = [
        'id',
        'name'=> 'like',
        'cpf_or_cnpj' => 'like',
    ];

    public function model()
    {
        return Fuel::class;
    }

    public function validator()
    {
        return FuelValidator::class;
    }

    public function presenter()
    {
        return FuelPresenter::class;
    }
}

<?php


namespace Api\Fuel\Presenters;


use Api\Fuel\Transformer\FuelTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class FuelPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'fuel';
    protected $resourceKeyCollection = 'fuels';

    public function getTransformer()
    {
        return new FuelTransformer();
    }
}

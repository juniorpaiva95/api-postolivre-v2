<?php

namespace Api\Fuel\Transformer;

use Api\Fuel\Models\Fuel;
use League\Fractal\TransformerAbstract;

class FuelTransformer extends TransformerAbstract
{
    protected $availableIncludes = [];

    public function transform(Fuel $model)
    {
        return [
            'id'                => $model->id,
            'identifier'        => $model->identifier,
            'name'              => $model->name,
            'color_hex'         => $model->color_hex,
            'slug'              => $model->slug,
            'created_at'        => $model->created_at,
            'updated_at'        => $model->updated_at,
            'deleted_at'        => $model->deleted_at
        ];
    }
}

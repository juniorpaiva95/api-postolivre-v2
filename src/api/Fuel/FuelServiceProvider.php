<?php

namespace Api\Fuel;

use Api\Fuel\Events\FuelWasCreated;
use Api\Fuel\Events\FuelWasDeleted;
use Api\Fuel\Events\FuelWasUpdated;
use Api\Fuel\Interfaces\IFuelRepository;
use Api\Fuel\Interfaces\IFuelService;
use Api\Fuel\Repositories\FuelRepository;
use Api\Fuel\Services\FuelService;
use Infrastructure\Listeners\LogUserActivity;
use Infrastructure\Providers\EventServiceProvider;

class FuelServiceProvider extends EventServiceProvider
{
    protected $listen = [
        FuelWasCreated::class => [
            LogUserActivity::class
        ],
        FuelWasDeleted::class => [
            LogUserActivity::class
        ],
        FuelWasUpdated::class => [
            LogUserActivity::class
        ],
    ];

    public function register()
    {
        $this->app->bind(IFuelService::class, FuelService::class);
        $this->app->bind(IFuelRepository::class, FuelRepository::class);
    }
}

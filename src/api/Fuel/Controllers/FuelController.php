<?php

namespace Api\Fuel\Controllers;

use Api\Fuel\Interfaces\IFuelService;
use Infrastructure\Http\CrudController;

/**
 * Class FuelController
 * @package Api\Fuel\Controllers
 */
class FuelController extends CrudController
{
    /**
     * FuelController constructor.
     * @param IFuelService $service
     */
    public function __construct(IFuelService $service)
    {
        parent::__construct($service);
    }
}

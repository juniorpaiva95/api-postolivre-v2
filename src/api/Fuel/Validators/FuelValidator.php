<?php
namespace Api\Fuel\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class FuelValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|string',
            'slug' => 'required|string',
            'color_hex' => 'required|string'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'string',
        ]
    ];

}

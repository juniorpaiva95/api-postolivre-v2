<?php

namespace Api\Lot\Services;

use Api\Auction\Interfaces\IAuctionRepository;
use Api\Lot\Events\LotWasCreated;
use Api\Lot\Events\LotWasDeleted;
use Api\Lot\Events\LotWasUpdated;
use Api\Lot\Interfaces\ILotRepository;
use Api\Lot\Interfaces\ILotService;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Infrastructure\Services\Service;

class LotService extends Service implements ILotService
{
    private $auctionRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        ILotRepository $repository,
        IAuctionRepository $auctionRepository
    )
    {
        parent::__construct($database, $dispatcher, $repository);
        $this->auctionRepository = $auctionRepository;

        $this->modelCreated = LotWasCreated::class;
        $this->modelUpdated = LotWasUpdated::class;
        $this->modelDeleted = LotWasDeleted::class;
        $this->autoIncrement = true;
    }

    private function getStartDate() : Carbon {
        if (env('APP_ENV') === 'homologacao' || env('APP_ENV') === 'production') {
            return Carbon::create(
                Carbon::now()->year,
                Carbon::now()->month,
                Carbon::now()->day, '12', '00');
        }
        return Carbon::create(
            Carbon::now()->year,
            Carbon::now()->month,
            Carbon::now()->day, '00', '00', '00');

    }

    private function getFinishDate() : Carbon {
        if (env('APP_ENV') === 'homologacao' || env('APP_ENV') === 'production') {
            return Carbon::create(
                Carbon::now()->year,
                Carbon::now()->month,
                Carbon::now()->day, '17', '00', '00');
        }
        return Carbon::create(
            Carbon::now()->year,
            Carbon::now()->month,
            Carbon::now()->day, '23', '59', '59');

    }

    public function gerarLoteDiario(): Collection
    {
        $lotesCriados = [];
        $this->database->beginTransaction();
        try {
            $lotesACriar = $this->auctionRepository->scopeQuery(function (Builder $query) {
                return $query->whereBetween('created_at', [
                    $this->getStartDate(), $this->getFinishDate()
                ])
                    ->whereNull('lot_id')
                    ->groupBy(['fuel_id', 'port_id']);
            })->selectRaw('fuel_id, port_id, SUM(fuel_amount)')->get();

            collect($lotesACriar)->each(function ($lote) use (&$lotesCriados){
                $loteCriado = $this->create([
                    'fuel_id' => $lote->fuel_id,
                    'port_id' => $lote->port_id
                ]);


                $lotesCriados[] = $loteCriado;
                /*
                 * Atualiza todos os pedidos/leiloes que foram criados na data padrao 12 as 17hrs
                 * e que pertencem ao grupo do lote corrente onde o lot_id e null
                 * */
                $this->auctionRepository->updateWhere([
                    ['created_at', '>=', $this->getStartDate()],
                    ['created_at', '<=', $this->getFinishDate()],
                    'fuel_id' => $lote->fuel_id,
                    'port_id' => $lote->port_id,
                    'lot_id' => null
                ], ['lot_id'=> $loteCriado->id]);
                $this->database->commit();
            });
        }catch (\Exception $exception) {
            $this->database->rollBack();
            Log::error("Error exception: " . $exception->getMessage());
            throw $exception;
        }

        return collect($lotesCriados);
    }
}

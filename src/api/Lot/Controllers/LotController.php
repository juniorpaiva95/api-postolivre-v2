<?php

namespace Api\Lot\Controllers;

use Api\Lot\Interfaces\ILotRepository;
use Api\Lot\Interfaces\ILotService;
use Illuminate\Database\Eloquent\Builder;
use Infrastructure\Http\CrudController;

/**
 * Class LotController
 * @package Api\Lot\Controllers
 */
class LotController extends CrudController
{
    private $lotRepository;
    /**
     * LotController constructor.
     * @param ILotService $service
     */
    public function __construct(ILotService $service, ILotRepository $lotRepository)
    {
        $this->lotRepository = $lotRepository;
        parent::__construct($service);
    }

    public function gerarLotes() {
        $lotes = $this->service->gerarLoteDiario();
        echo "Lotes gerados - " . count($lotes);
    }

    public function index()
    {
        $limit = app('request')->query('limit');
        $includes = explode(',', app('request')->query('include'));
        $search = app('request')->query('search');

        $searchFilters = $this->parserSearchData($search);
//        dd($search, $searchFilters);
        $filtroData = isset($searchFilters['created_at']) ? $searchFilters['created_at'] : null;
        $filtroPosto = isset($searchFilters['auctions.station.user.social_reason']) ? $searchFilters['auctions.station.user.social_reason']: null;
        $filtroCombustivel = isset($searchFilters['fuel.id']) ? $searchFilters['fuel.id'] : null;

        $this->lotRepository->skipCriteria();

        $q = $this->lotRepository->scopeQuery(function ($query) use (
            $searchFilters,
            $filtroData,
            $filtroPosto,
            $filtroCombustivel,
            $limit,
            $includes
        ) {
           $q = $query
                ->join('fuels', 'fuels.id', '=', 'lots.fuel_id')
                ->join('ports', 'ports.id', '=', 'lots.port_id')
                ->join('auctions', 'auctions.lot_id', '=', 'lots.id')
                ->join('stations', 'stations.id', '=', 'auctions.station_id')
                ->join('users', 'users.id', '=', 'stations.user_id')
                ->with($includes)
                ->select('lots.*');

           if (isset($searchFilters['fuel.name']))
               $q->whereIn('fuels.name',  explode(',', $searchFilters['fuel.name']));

           if (isset($filtroCombustivel)) {
               $q->where('fuels.id', '=', $filtroCombustivel);
           }
           if (isset($filtroPosto)) {
               $q->where(function ($queryScope) use ($filtroPosto) {
                   $queryScope->where('users.social_reason', 'like', "%{$filtroPosto}%")
                       ->orWhere('users.cnpj', 'like', "%{$filtroPosto}%");
               });
           }
           if (isset($filtroData)) {
               $dataInicial = explode(',', $filtroData)[0];
               $dataFinal = explode(',', $filtroData)[1];

               $q->whereBetween('lots.created_at', [$dataInicial, $dataFinal]);
           }
           if ($limit) {
               $q->limit($limit);
           }
           return $q;
        });
//
//dd($q->get());
//        dd(vsprintf(str_replace('?', '%s', $q->toSql()), collect($q->getBindings())->map(function($binding){
//            return is_numeric($binding) ? $binding : "'{$binding}'";
//        })->toArray()));
        return $this->response($q->get());
    }

    private function parserSearchData($search)
    {
        $searchData = [];

        if (stripos($search, ':')) {
            $fields = explode(';', $search);

            foreach ($fields as $row) {
                try {
                    list($field, $value) = explode(':', $row);
                    $searchData[$field] = $value;
                } catch (\Exception $e) {
                    //Surround offset error
                }
            }
        }

        return $searchData;
    }
}

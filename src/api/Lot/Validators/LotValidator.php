<?php
namespace Api\Lot\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class LotValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'port_id' => 'required|exists:ports,id',
            'fuel_id' => 'required|exists:fuels,id',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'port_id' => 'exists:ports,id',
            'fuel_id' => 'exists:fuels,id'
        ]
    ];

}

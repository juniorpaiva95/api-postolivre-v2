<?php

namespace Api\Lot\Repositories;

use Api\Lot\Interfaces\ILotRepository;
use Api\Lot\Models\Lot;
use Api\Lot\Presenters\LotPresenter;
use Api\Lot\Validators\LotValidator;
use Infrastructure\Database\Eloquent\Repository;


class LotRepository extends Repository implements ILotRepository
{

    protected $fieldSearchable = [
        'id',
    ];

    public function model()
    {
        return Lot::class;
    }

    public function validator()
    {
        return LotValidator::class;
    }

    public function presenter()
    {
        return LotPresenter::class;
    }
}

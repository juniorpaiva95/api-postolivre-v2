<?php

namespace Api\Lot\Transformers;

use Api\Auction\Models\Auction;
use Api\Auction\Transformer\AuctionTransformer;
use Api\Bid\Models\Bid;
use Api\Bid\Transformer\BidTransformer;
use Api\Fuel\Models\Fuel;
use Api\Fuel\Transformer\FuelTransformer;
use Api\Lot\Models\Lot;
use Api\Port\Models\Port;
use Api\Port\Transformers\PortTransformer;
use League\Fractal\TransformerAbstract;

class LotTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['fuel', 'port', 'auctions', 'bids'];

    public function transform(Lot $model)
    {
        return [
            'id'                => $model->id,
            'identifier'        => $model->identifier,
            'port_id'           => $model->port_id,
            'fuel_id'           => $model->fuel_id,
            'created_at'        => $model->created_at,
            'updated_at'        => $model->updated_at,
            'deleted_at'        => $model->deleted_at
        ];
    }

    public function includeFuel(Lot $model)
    {
        if (null === $model->fuel) {
            return $this->null();
        }

        return $this->item($model->fuel, new FuelTransformer(), resourceKey(Fuel::class));
    }

    public function includePort(Lot $model)
    {
        if (null === $model->port) {
            return $this->null();
        }

        return $this->item($model->port, new PortTransformer(), resourceKey(Port::class));
    }

    public function includeAuctions(Lot $model)
    {
        if (null === $model->auctions) {
            return $this->null();
        }

        return $this->collection($model->auctions, new AuctionTransformer(), resourceKey(Auction::class));
    }

    public function includeBids(Lot $model)
    {
        if (null === $model->bids) {
            return $this->null();
        }

        return $this->collection($model->bids, new BidTransformer(), resourceKey(Bid::class));
    }
}

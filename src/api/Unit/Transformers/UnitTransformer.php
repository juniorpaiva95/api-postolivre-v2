<?php

namespace Api\Unit\Transformer;

use Api\Unit\Models\Unit;
use Api\User\Models\User;
use Api\User\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

class UnitTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['users'];

    public function transform(Unit $model)
    {
        return [
            'id'                => $model->id,
            'identifier'        => $model->identifier,
            'name'              => $model->name,
            'cpf_or_cnpj'       => $model->cpf_or_cnpj,
            'email'       => $model->email,
            'state_abbreviation'=> $model->state_abbreviation,
            'phone'             => $model->phone,
            'created_at'        => $model->created_at,
            'updated_at'        => $model->updated_at,
            'deleted_at'        => $model->deleted_at
        ];
    }

    public function includeUsers(Unit $model)
    {
        if ($model->users === null) {
            return $this->null();
        }

        return $this->collection($model->users, new UserTransformer(), resourceKey(User::class));
    }
}

<?php

namespace Api\Unit\Controllers;

use Api\Unit\Interfaces\IUnitService;
use Infrastructure\Http\CrudController;

/**
 * Class UnitController
 * @package Api\Unit\Controllers
 */
class UnitController extends CrudController
{
    /**
     * UnitController constructor.
     * @param IUnitService $service
     */
    public function __construct(IUnitService $service)
    {
        parent::__construct($service);
    }
}

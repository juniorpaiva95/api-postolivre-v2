<?php
use Illuminate\Support\Facades\Route;

Route::get('/api/v1/units', 'UnitController@index')->name('units.index');

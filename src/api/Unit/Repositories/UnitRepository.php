<?php

namespace Api\Unit\Repositories;

use Api\Unit\Interfaces\IUnitRepository;
use Api\Unit\Models\Unit;
use Api\Unit\Presenters\UnitPresenter;
use Api\Unit\Validators\UnitValidator;
use Infrastructure\Database\Eloquent\Repository;


class UnitRepository extends Repository implements IUnitRepository
{

    protected $fieldSearchable = [
        'id',
        'name'=>'like',
        'cpf_or_cnpj' => 'like',
        'state_abbreviation' => 'like',
    ];

    public function model()
    {
        return Unit::class;
    }

    public function validator()
    {
        return UnitValidator::class;
    }

    public function presenter()
    {
        return UnitPresenter::class;
    }
}

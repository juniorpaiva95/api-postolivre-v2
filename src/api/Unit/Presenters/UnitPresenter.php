<?php


namespace Api\Unit\Presenters;


use Api\Unit\Transformer\UnitTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class UnitPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'units';
    protected $resourceKeyCollection = 'units';

    public function getTransformer()
    {
        return new UnitTransformer();
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('uuid');
            $table->uuid('user_id')->nullable();

            //when a upload has specific place eg. profile image
            $table->string('type', 50)->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();

            $table->string('filename');

            $table->string('external_url')->nullable();//externo
            $table->string('external_bucket')->nullable();//bucket
            $table->string('external_endpoint')->nullable();//endpoint
            $table->string('provider')->nullable();//local, spaces, s3 etc

            $table->string('mime', 50);
            $table->string('extension', 10);
            $table->unsignedInteger('size')->nullable();

            $table->uuid('uploadable_id');
            $table->string('uploadable_type');

            $table->boolean('is_public')->default(true);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploads');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Infrastructure\Enums\FreightTypeEnum;
use Infrastructure\Enums\PickupLocationTypeEnum;

class CreateAuctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auctions', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('station_id');
            $table->uuid('fuel_id');
            $table->uuid('port_id');
            $table->uuid('lot_id')->nullable();

            $table->unsignedBigInteger('identifier');
            $table->enum('freight_type', [
                FreightTypeEnum::CIF,
                FreightTypeEnum::FOB
            ])->index();
            $table->unsignedInteger('fuel_amount');

            $table->dateTime('date_start');
            $table->dateTime('date_finish');

            $table->timestamps();
            $table->softDeletes();

            $table->dateTime('payment_validated_at')->nullable();

            $table->primary('id');
            $table->foreign('station_id')->references('id')->on('stations')->onDelete('cascade');
            $table->foreign('fuel_id')->references('id')->on('fuels')->onDelete('cascade');
            $table->foreign('port_id')->references('id')->on('ports')->onDelete('cascade');
            $table->foreign('lot_id')->references('id')->on('lots')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auctions');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faqs_question', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('user_id');

            $table->unsignedBigInteger('identifier');

            $table->text('question')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->primary('id');

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('faqs_answer', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('user_id');
            $table->uuid('question_id');

            $table->unsignedBigInteger('identifier');

            $table->text('answer')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->primary('id');

            $table->foreign('question_id')->references('id')->on('faqs_question')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faqs_answer');
        Schema::dropIfExists('faqs_question');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id');

            $table->uuid('unit_id');
            $table->uuid('address_id')->index()->nullable();

            $table->unsignedBigInteger('identifier');

            $table->string('social_reason')->unique();
            $table->string('state_registration')->unique();
            $table->string('cnpj')->unique();

            $table->string('phone_number')->nullable();

            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('confirmed')->default(false);
            $table->boolean('is_super')->default(false)->nullable();
            $table->timestamp('email_verified_at')->nullable();

            $table->rememberToken();
            $table->timestamp('latest_login')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->primary('id');
            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');
            $table->foreign('address_id')->references('id')->on('address')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

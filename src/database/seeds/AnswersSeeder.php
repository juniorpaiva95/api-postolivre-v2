<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class AnswersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions = DB::table('faqs_question')
            ->get(['id'])
            ->take(10);

        $user = DB::table('users')->first(['id']);

        foreach ($questions as $question) {
            factory(\Api\Faq\Models\Answer::class, random_int(1,2))->create([
                'question_id' => $question->id,
                'answer' => \Faker\Provider\Lorem::paragraph(),
                'user_id' => $user->id
            ]);
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Api\Auction\Models\Auction;
use Carbon\Carbon;

class AuctionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stations = DB::table('stations')->get(['id'])->take(2);
        $fuels = DB::table('fuels')->get(['id'])->take(5);
        $port = DB::table('ports')->get(['id'])->first();

        $index = 0;
        foreach ($stations as $station) {
            foreach ($fuels as $fuel) {
                $index++;
                factory(Auction::class)->create([
                    'identifier' => $index,
                    'station_id' => $station->id,
                    'fuel_id' => $fuel->id,
                    'port_id' => $port->id,
                    'created_at' => Carbon::now()->setHour(12)
                ]);
            }
        }
    }
}

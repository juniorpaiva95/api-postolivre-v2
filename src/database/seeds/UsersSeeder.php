<?php

use Illuminate\Database\Seeder;
use Api\User\Models\User;
use Api\User\Models\Address;
use Illuminate\Support\Facades\Hash;
use Faker\Generator as Faker;
use Api\User\Interfaces\IUserService;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Infrastructure\Enums\StateEnum;

class UsersSeeder extends Seeder
{
    private $faker;

    private $userService;

    public function __construct(Faker $faker, IUserService $userService)
    {
        $this->userService = $userService;
        $this->faker = $faker;
        $this->faker->addProvider(new \Faker\Provider\pt_BR\Person($faker));
        $this->faker->addProvider(new \Faker\Provider\pt_BR\Company($faker));
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create roles and assign existing permissions
        $unidade = \Api\Unit\Models\Unit::query()->first();
//        $role_admin = \Api\User\Models\Role::where('name', 'admin')->first();
        $role_station = \Api\User\Models\Role::where('name', 'gas_station')->first();
        $role_distributor = \Api\User\Models\Role::where('name', 'distributor')->first();

        /**
         * $admin User
         */
        $admin = $this->userService->create([
            'email' => 'admin@postolivre.com',
            'social_reason' => $this->faker->company,
            'state_registration' => $this->faker->randomNumber(8),
            'password' => Hash::make('secret'),
            'phone_number' => $this->faker->phoneNumber,
            'cnpj' => $this->faker->cnpj,
            'unit_id' => $unidade->id,
            'confirmed' => true,
            'email_verified_at' => \Carbon\Carbon::now(),
            "type" => 'admin',
            "address" => [
                "state" => StateEnum::PB,
                "city" => "{$this->faker->city}",
                "street" => "{$this->faker->streetName}",
                "number" => "{$this->faker->randomNumber(4)}",
                "neighborhood" => 'Manaira',
                "cep" => "{$this->faker->postcode}"
            ]
        ]);

//        if ($role_admin) {
//            $admin->assignRole($role_admin);
//        }
        if (!in_array(env('APP_ENV'), ['production'])) {
            $distributor = $this->userService->create([
                "email" => "distribuidor@postolivre.com",
                "cnpj" => "01.551.865/0001-09",
                "social_reason" => "{$this->faker->company}",
                "type" => "distributor",
                "state_registration" => "{$this->faker->randomNumber(8)}",
                "unit_id" => $unidade->id,
                'confirmed' => true,
                'email_verified_at' => \Carbon\Carbon::now(),
                'password' => Hash::make('secret'),
                'phone_number' => $this->faker->phoneNumber,
                "address" => [
                    "state" => StateEnum::PB,
                    "city" => "{$this->faker->city}",
                    "street" => "{$this->faker->streetName}",
                    "number" => "{$this->faker->randomNumber(4)}",
                    "neighborhood" => 'Manaira',
                    "cep" => "{$this->faker->postcode}"
                ],
                "bank" => [
                    "code" => "01",
                    "agency" => "1010",
                    "account" => "25035",
                    "type" => "13"
                ]
            ]);

            $distributor2 = $this->userService->create([
                "email" => "distribuidor2@postolivre.com",
                "cnpj" => "52.528.980/0001-01",
                "social_reason" => "{$this->faker->company}",
                "type" => "distributor",
                "state_registration" => "{$this->faker->randomNumber(8)}",
                "unit_id" => $unidade->id,
                'confirmed' => true,
                'email_verified_at' => \Carbon\Carbon::now(),
                'password' => Hash::make('secret'),
                'phone_number' => $this->faker->phoneNumber,
                "address" => [
                    "state" => StateEnum::PB,
                    "city" => "{$this->faker->city}",
                    "street" => "{$this->faker->streetName}",
                    "number" => "{$this->faker->randomNumber(4)}",
                    "neighborhood" => 'Manaira',
                    "cep" => "{$this->faker->postcode}"
                ],
                "bank" => [
                    "code" => "01",
                    "agency" => "1010",
                    "account" => "25035",
                    "type" => "13"
                ]
            ]);

            if ($role_distributor) {
                $distributor->assignRole($role_distributor);
                $distributor2->assignRole($role_distributor);
            }

            $posto = $this->userService->create([
                "email" => "posto@postolivre.com",
                "cnpj" => "33.093.434/0001-32",
                "social_reason" => "{$this->faker->company}",
                "type" => "station",
                "state_registration" => "{$this->faker->randomNumber(8)}",
                "unit_id" => $unidade->id,
                'confirmed' => true,
                'email_verified_at' => \Carbon\Carbon::now(),
                'password' => Hash::make('secret'),
                'phone_number' => $this->faker->phoneNumber,
                "address" => [
                    "state" => StateEnum::PB,
                    "city" => "{$this->faker->city}",
                    "street" => "{$this->faker->streetName}",
                    "number" => "{$this->faker->randomNumber(4)}",
                    "neighborhood" => 'Manaira',
                    "cep" => "{$this->faker->postcode}"
                ],
                'file' => [

                    $this->pathToUploadedFile(public_path('sample.jpeg')),
                    $this->pathToUploadedFile(public_path('sample.jpeg'))
                ]
            ]);

            $posto2 = $this->userService->create([
                "email" => "posto2@postolivre.com",
                "cnpj" => "83.348.921/0001-77",
                "social_reason" => "{$this->faker->company}",
                "type" => "station",
                "state_registration" => "{$this->faker->randomNumber(8)}",
                "unit_id" => $unidade->id,
                'confirmed' => true,
                'email_verified_at' => \Carbon\Carbon::now(),
                'password' => Hash::make('secret'),
                'phone_number' => $this->faker->phoneNumber,
                "address" => [
                    "state" => StateEnum::PB,
                    "city" => "{$this->faker->city}",
                    "street" => "{$this->faker->streetName}",
                    "number" => "{$this->faker->randomNumber(4)}",
                    "neighborhood" => 'Manaira',
                    "cep" => "{$this->faker->postcode}"
                ],
                'file' => [
                    $this->pathToUploadedFile(public_path('sample.jpeg')),
                    $this->pathToUploadedFile(public_path('sample.jpeg'))
                ]
            ]);

            if ($role_station) {
                $posto->assignRole($role_station);
                $posto2->assignRole($role_station);
            }
        }


    }

    /**
     * Create an UploadedFile object from absolute path
     *
     * @static
     * @param string $path
     * @param bool $public default false
     * @return    object(Symfony\Component\HttpFoundation\File\UploadedFile)
     * @author    Alexandre Thebaldi
     */

    private function pathToUploadedFile($path, $public = false)
    {
        $name = File::name($path);
        $extension = File::extension($path);
        $originalName = $name . '.' . $extension;
        $mimeType = File::mimeType($path);
        $error = null;
        $test = $public;
        $object = new UploadedFile($path, $originalName, $mimeType, $error, $test);

        return $object;
    }
}

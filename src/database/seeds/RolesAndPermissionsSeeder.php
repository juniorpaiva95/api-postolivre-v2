<?php

use Illuminate\Database\Seeder;
use Api\User\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');

//        Role::create(['name' => 'admin','label' =>'Administrador','guard_name' => 'api']);
        Role::create(['name' => 'admin', 'guard_name' => 'api']);
        Role::create(['name' => 'gas_station', 'guard_name' => 'api']);
        Role::create(['name' => 'distributor', 'guard_name' => 'api']);
    }
}

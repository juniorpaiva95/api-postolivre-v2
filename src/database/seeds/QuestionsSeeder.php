<?php

use Illuminate\Database\Seeder;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \Illuminate\Support\Facades\DB::table('users')->first(['id']);
        $questions = [
            [
                'question' => 'O POSTO LIVRE participa ou tem qualquer ingerência na negociação entre
distribuidoras e revendedores de combustíveis?',
                'answer' => 'Não. O POSTO LIVRE apenas disponibiliza o ambiente virtual (marketplace) para as
distribuidoras e revendedores de combustíveis realizarem suas negociações, não
possuindo, portanto, nenhuma interferência no pedido, na formação de compra
coletiva, nem nos lances apresentados.'
            ],
            [
                'question' => 'O cadastro no POSTO LIVRE é gratuito?',
                'answer' => 'Sim. Para se cadastrar no POSTO LIVRE, as distribuidoras e os revendedores de
combustíveis não pagam qualquer valor.'
            ],
            [
                'question' => 'Como os revendedores de combustíveis apresentam o pedido de aquisição de
produtos?',
                'answer'   => 'Após feito o login, os revendedores de combustíveis vão ao campo específico do
pedido e optam pelo combustível a ser adquirido, a quantidade de combustível, a
opção pelo frete FOB (Free On Board) e o local de retirada do produto.'
            ],
            [
                'question'=> 'Há possibilidade de variação no valor do litro do combustível decorrente da
tributação aplicada em cada Estado?',
                'answer'    =>  'Sim. Tributos a exemplo do ICMS, a formação de pauta fiscal (base de cálculo
presumida), dentre outras questões podem variar o valor do litro do combustível em
cada Estado.'
            ],
            [
                'question' => 'Como as distribuidoras de combustíveis ofertam os lances?',
                'answer' => 'Feito o login e após o tempo de formulação dos pedidos, haverá pedido de compra
coletiva de determinada quantidade um referido combustível, a ser retirado no
terminal indicado na compra coletiva (exemplo: 100 mil litros de gasolina aditivada a
serem retirados no Terminal de Suape/PE). Caso a distribuidora tenha interesse em
participar da negociação, ofertará lances de menor preço por litro de combustível,
dentro do limite de tempo estabelecido na plataforma digital.',
            ],
            [
                'question' => 'O revendedor de combustíveis pode desistir do pedido?',
                'answer'    => 'Enquanto o tempo para formulação dos pedidos estiver em aberto, o revendedor de
combustíveis pode desistir do pedido. Encerrado o período de tempo destinado à
formulação dos pedidos, o revendedor de combustíveis não mais poderá cancelá-lo.'
            ],
            [
                'question' => 'A distribuidora de combustíveis pode desistir do leilão?',
                'answer'    => 'A distribuidora de combustíveis, uma vez ofertado um lance de menor preço, não
poderá desistir da negociação. Todavia, se outra distribuidora der um lance menor,
aquela primeira oferta perderá o objeto, nos moldes dos leilões de menor preço.'
            ],
            [
                'question' => 'Qual a duração do leilão?',
                'answer'    => 'Uma vez fechado o período de formulação de pedido de compra coletiva, as
distribuidoras de combustíveis poderão ofertar lances de menor preço entre às 17h e
às 9h do dia seguinte.'
            ],
            [
                'question' => 'O POSTO LIVRE tem responsabilidade pela entrega do produto?',
                'answer'    => 'Não. O POSTO LIVRE não possui qualquer responsabilidade pela entrega do produto,
cabendo à distribuidora dispor em estoque e assegurar a entrega da quantidade de
combustível indicada na compra coletiva, e ao revendedor providenciar a sua retirada
em até 48h (quarenta e oito horas) a partir da efetuação do pagamento no Terminal de
retirada indicado (frete FOB - Free On Board).'
            ],
            [
                'question' => 'O POSTO LIVRE tem responsabilidade pelo pagamento do pedido?',
                'answer'    => 'Não. O POSTO LIVRE não possui qualquer ingerência na negociação entre
distribuidoras e revendedores de combustíveis, portanto, não possui qualquer
responsabilidade pelo pagamento do produto.'
            ],
            [
                'question' => 'O POSTO LIVRE negocia algum produto?',
                'answer'    => 'Não. O POSTO LIVRE não negocia, em hipótese alguma, qualquer produto. O POSTO
LIVRE apenas disponibiliza um ambiente virtual de negócios (marketplace) que viabiliza
e facilita o encontro entre distribuidoras e revendedores de combustíveis.'
            ],
            [
                'question' => 'Quais são os produtos negociados entre distribuidoras e revendedores de
combustíveis no ambiente virtual de negócios (marketplace) disponibilizado pelo
POSTO LIVRE?',
                'answer'    => 'Gasolina Comum, Gasolina Aditivada, Etanol, Diesel S10 e Diesel S500.'
            ]
        ];
        $index = 0;
        collect($questions)->each(function ($question, $key) use ($user, $questions, &$index) {
            $index++;
            $question = factory(\Api\Faq\Models\Question::class)->create([
                "identifier" => $index,
                "question" => $question['question'],
                "user_id" => $user->id
            ]);
            factory(\Api\Faq\Models\Answer::class)->create([
                'identifier' => $index,
               'question_id' => $question->id,
               'user_id' => $user->id,
               'answer' =>  $questions[$key]['answer']
            ]);
        });

    }
}

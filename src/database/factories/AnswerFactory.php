<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Api\Faq\Models\Answer;
use Faker\Generator as Faker;

$factory->define(Answer::class, function (Faker $faker) {
    static $identifier;
    return [
        'answer' => \Faker\Provider\Lorem::paragraph(),
        'identifier' => $identifier
    ];
});

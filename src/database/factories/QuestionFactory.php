<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Api\Faq\Models\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    static $identifier;
    return [
        "identifier" => $identifier,
        "question" => \Faker\Provider\Lorem::sentence(10, true)
    ];
});

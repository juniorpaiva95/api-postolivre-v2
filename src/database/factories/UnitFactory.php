<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Api\Unit\Models\Unit;
use Faker\Generator as Faker;
use Faker\Provider\pt_BR\Person as FakerBrPerson;
use Faker\Provider\pt_BR\Company as FakerBrCompany;

$factory->define(Unit::class, function (Faker $faker) {
    $faker->addProvider(new FakerBrPerson($faker));
    $faker->addProvider(new FakerBrCompany($faker));

    $cpfOrCnpj = random_int(0, 1) ? $faker->cpf() : $faker->cnpj();

    return [
        'slug' => $faker->company,
        'code' => $faker->uuid,
        'name' => 'Unidade '. $faker->unique()->jobTitle,
        'cpf_or_cnpj' => $cpfOrCnpj,
        'phone' => 9 . $faker->randomNumber(8),
        'email' => $faker->unique()->email,
        'active' => $faker->boolean(true)
    ];
});

<?php

use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Infrastructure\Auth\Exceptions\InvalidCredentialsException;
use Infrastructure\Exceptions\Formatters\ExceptionFormatter;
use Infrastructure\Exceptions\Formatters\InvalidCredentialsExceptionFormatter;
use Infrastructure\Exceptions\Formatters\ModelNotFoundExceptionFormatter;
use Infrastructure\Exceptions\Formatters\NotFoundHttpExceptionFormatter;
use Infrastructure\Exceptions\Formatters\QueryExceptionFormatter;
use Infrastructure\Exceptions\Formatters\UnauthenticatedExceptionFormatter;
use Infrastructure\Exceptions\Formatters\UnauthorizedExceptionFormatter;
use Infrastructure\Exceptions\Formatters\UnprocessableEntityHttpExceptionFormatter;
use Infrastructure\Exceptions\Formatters\ValidatorExceptionFormatter;

use Prettus\Validator\Exceptions\ValidatorException;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

return [
    'add_cors_headers' => false,

    // Has to be in prioritized order, e.g. highest priority first.
    'formatters' => [
        QueryException::class => QueryExceptionFormatter::class,
        UnprocessableEntityHttpException::class => UnprocessableEntityHttpExceptionFormatter::class,
        NotFoundHttpException::class => NotFoundHttpExceptionFormatter::class,
        ModelNotFoundException::class => ModelNotFoundExceptionFormatter::class,
        Illuminate\Validation\ValidationException::class => ValidatorExceptionFormatter::class,
        ValidatorException::class => ValidatorExceptionFormatter::class,
        InvalidCredentialsException::class => InvalidCredentialsExceptionFormatter::class,
        \Spatie\Permission\Exceptions\UnauthorizedException::class => UnauthorizedExceptionFormatter::class,
        AuthenticationException::class => UnauthenticatedExceptionFormatter::class,
        Exception::class => ExceptionFormatter::class,
        \Illuminate\Broadcasting\BroadcastException::class => ExceptionFormatter::class
    ],

    'response_factory' => \Infrastructure\Exceptions\ResponseFactory::class,

    'reporters' => [
        //'sentry' => [
          //'class'  => SentryReporter::class,
          //'config' => [
          //    'dsn' => env('SENTRY_DSN', ''),
              // For extra options see https://docs.sentry.io/clients/php/config/
              // php version and environment are automatically added.
              //'sentry_options' => []
          //]
      //]
    ],

    'server_error_production' => 'An error occurred.'
];

<?php

return [

    'email' => [
        'confirmation' => [
            'from' => env('EMAIL_CONFIRMATION_FROM', 'noreply@postolivre.com'),
            'link' => env('EMAIL_CONFIRMATION_LINK', 'reset-password'),
        ],
        'reset_password' => [
            'from' => env('EMAIL_CONFIRMATION_FROM', 'ti@stalo.digital'),
            'link' => env('EMAIL_RESET_PASSWORD_LINK', 'reset-password'),
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'mail' => [
        'sendgrid' => [
            'api_key' => env('SENDGRID_API_KEY', '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')
        ]
    ]

];

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><meta name="format-detection" content="telephone=no" /><title>Confirmação Email</title><link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet" type="text/css" /><style type="text/css">
        /* Resets */
        .ReadMsgBody { width: 100%; background-color: #ebebeb;}
        .ExternalClass {width: 100%; background-color: #ebebeb;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
        a[x-apple-data-detectors]{
            color:inherit !important;
            text-decoration:none !important;
            font-size:inherit !important;
            font-family:inherit !important;
            font-weight:inherit !important;
            line-height:inherit !important;
        }
        body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
        body {margin:0; padding:0;}
        .yshortcuts a {border-bottom: none !important;}
        .rnb-del-min-width{ min-width: 0 !important; }

        /* Add new outlook css start */
        .templateContainer{
            max-width:590px !important;
            width:auto !important;
        }
        /* Add new outlook css end */

        /* Image width by default for 3 columns */
        img[class="rnb-col-3-img"] {
            max-width:170px;
        }

        /* Image width by default for 2 columns */
        img[class="rnb-col-2-img"] {
            max-width:264px;
        }

        /* Image width by default for 2 columns aside small size */
        img[class="rnb-col-2-img-side-xs"] {
            max-width:180px;
        }

        /* Image width by default for 2 columns aside big size */
        img[class="rnb-col-2-img-side-xl"] {
            max-width:350px;
        }

        /* Image width by default for 1 column */
        img[class="rnb-col-1-img"] {
            max-width:550px;
        }

        /* Image width by default for header */
        img[class="rnb-header-img"] {
            max-width:590px;
        }

        /* Ckeditor line-height spacing */
        .rnb-force-col p, ul, ol{margin:0px!important;}
        .rnb-del-min-width p, ul, ol{margin:0px!important;}

        /* tmpl-2 preview */
        .rnb-tmpl-width{ width:100%!important;}

        /* tmpl-11 preview */
        .rnb-social-width{padding-right:15px!important;}

        /* tmpl-11 preview */
        .rnb-social-align{float:right!important;}

        @media only screen and (min-width:590px){
            /* mac fix width */
            .templateContainer{width:590px !important;}
        }

        @media screen and (max-width: 360px){
            /* yahoo app fix width "tmpl-2 tmpl-10 tmpl-13" in android devices */
            .rnb-yahoo-width{ width:360px!important;}
        }

        @media screen and (max-width: 380px){
            /* fix width and font size "tmpl-4 tmpl-6" in mobile preview */
            .element-img-text{ font-size:24px!important;}
            .element-img-text2{ width:230px!important;}
            .content-img-text-tmpl-6{ font-size:24px!important;}
            .content-img-text2-tmpl-6{ width:220px!important;}
        }

        @media screen and (max-width: 480px) {
            td[class="rnb-container-padding"] {
                padding-left: 10px !important;
                padding-right: 10px !important;
            }

            /* force container nav to (horizontal) blocks */
            td[class="rnb-force-nav"] {
                display: block;
            }
        }

        @media only screen and (max-width : 600px) {

            /* center the address &amp; social icons */
            .rnb-text-center {text-align:center !important;}

            /* force container columns to (horizontal) blocks */
            td[class~="rnb-force-col"] {
                display: block;
                padding-right: 0 !important;
                padding-left: 0 !important;
                width:100%;
            }

            table[class~="rnb-container"] {
                width: 100% !important;
            }

            table[class="rnb-btn-col-content"] {
                width: 100% !important;
            }
            table[class="rnb-col-3"] {
                /* unset table align="left/right" */
                float: none !important;
                width: 100% !important;

                /* change left/right padding and margins to top/bottom ones */
                margin-bottom: 10px;
                padding-bottom: 10px;
                /*border-bottom: 1px solid #eee;*/
            }

            table[class="rnb-last-col-3"] {
                /* unset table align="left/right" */
                float: none !important;
                width: 100% !important;
            }

            table[class~="rnb-col-2"] {
                /* unset table align="left/right" */
                float: none !important;
                width: 100% !important;

                /* change left/right padding and margins to top/bottom ones */
                margin-bottom: 10px;
                padding-bottom: 10px;
                /*border-bottom: 1px solid #eee;*/
            }

            table[class="rnb-col-2-noborder-onright"] {
                /* unset table align="left/right" */
                float: none !important;
                width: 100% !important;

                /* change left/right padding and margins to top/bottom ones */
                margin-bottom: 10px;
                padding-bottom: 10px;
            }

            table[class="rnb-col-2-noborder-onleft"] {
                /* unset table align="left/right" */
                float: none !important;
                width: 100% !important;

                /* change left/right padding and margins to top/bottom ones */
                margin-top: 10px;
                padding-top: 10px;
            }

            table[class="rnb-last-col-2"] {
                /* unset table align="left/right" */
                float: none !important;
                width: 100% !important;
            }

            table[class="rnb-col-1"] {
                /* unset table align="left/right" */
                float: none !important;
                width: 100% !important;
            }

            img[class="rnb-col-3-img"] {
                /**max-width:none !important;**/
                width:100% !important;
            }

            img[class="rnb-col-2-img"] {
                /**max-width:none !important;**/
                width:100% !important;
            }

            img[class="rnb-col-2-img-side-xs"] {
                /**max-width:none !important;**/
                width:100% !important;
            }

            img[class="rnb-col-2-img-side-xl"] {
                /**max-width:none !important;**/
                width:100% !important;
            }

            img[class="rnb-col-1-img"] {
                /**max-width:none !important;**/
                width:100% !important;
            }

            img[class="rnb-header-img"] {
                /**max-width:none !important;**/
                width:100% !important;
                margin:0 auto;
            }

            img[class="rnb-logo-img"] {
                /**max-width:none !important;**/
                width:100% !important;
            }

            td[class="rnb-mbl-float-none"] {
                float:inherit !important;
            }

            .img-block-center{text-align:center!important;}

            .logo-img-center
            {
                float:inherit!important;
            }

            /* tmpl-11 preview */
            .rnb-social-align{margin:0 auto!important; float:inherit!important;}

            /* tmpl-11 preview */
            .rnb-social-center{display:inline-block;}

            /* tmpl-11 preview */
            .social-text-spacing{margin-bottom:0px!important; padding-bottom:0px!important;}

            /* tmpl-11 preview */
            .social-text-spacing2{padding-top:15px!important;}

        }@media screen{body{font-family:'Quicksand','Verdana',Geneva,sans-serif;}}</style><!--[if gte mso 11]><style type="text/css">table{border-spacing: 0; }table td {border-collapse: separate;}</style><![endif]--><!--[if !mso]><!--><style type="text/css">table{border-spacing: 0;} table td {border-collapse: collapse;}</style> <!--<![endif]--><!--[if gte mso 15]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--><!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head><body>

<table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#ffffff" style="background-color:#ffffff;">

    <tbody><tr style="display:none !important; font-size:1px; mso-hide: all;"><td></td><td></td></tr>
    <tr>
        <td align="center" valign="top">
            <!--[if gte mso 9]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="590" style="width:590px;">
                <tr>
                    <td align="center" valign="top" width="590" style="width:590px;">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
                <tbody><tr>

                    <td align="center" valign="top" bgcolor="#ffffff" style="background-color:#ffffff;">

                        <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px; background-color:#ffffff;" name="Layout_2620" id="Layout_2620">
                            <tbody><tr>
                                <td class="rnb-del-min-width" valign="top" align="center" bgcolor="#ffffff" style="min-width:590px; background-color:#ffffff;">
                                    <table width="100%" cellpadding="0" border="0" height="30" cellspacing="0" bgcolor="#ffffff" style="background-color:#ffffff;">
                                        <tbody><tr>
                                            <td valign="top" height="30">
                                                <img width="20" height="30" style="display:block; max-height:30px; max-width:20px;" alt="" src="http://img.mailinblue.com/new_images/rnb/rnb_space.gif">
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                    </td>
                </tr><tr>

                    <td align="center" valign="top" bgcolor="#ffffff" style="background-color:#ffffff;">

                        <div>
                            <!--[if mso]>
                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                            <![endif]-->

                            <!--[if mso]>
                            <td valign="top" width="590" style="width:590px;">
                            <![endif]-->
                            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#ffffff" style="min-width:590px; background-color:#ffffff;" name="Layout_1" id="Layout_1">
                                <tbody><tr>
                                    <td class="rnb-del-min-width" align="center" valign="top" bgcolor="#ffffff" style="min-width:590px; background-color: #ffffff;">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate;">
                                            <tbody><tr>
                                                <td height="40" style="font-size:1px; line-height:1px;"> </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="rnb-container-padding" bgcolor="#ffffff" style="background-color: #ffffff;" align="left">
                                                    <table width="100%" cellpadding="0" border="0" align="center" cellspacing="0">
                                                        <tbody><tr>
                                                            <td valign="top" align="center">
                                                                <table cellpadding="0" border="0" align="left" cellspacing="0" class="logo-img-center">
                                                                    <tbody><tr>
                                                                        <td valign="middle" align="center">
                                                                            <div style="border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block; " cellspacing="0" cellpadding="0" border="0"><div><img width="186" vspace="0" hspace="0" border="0" style="float: left;max-width:186px;display:block;" class="rnb-logo-img" src="data:image/png;base64,{{$logo}}"></div></div></td>
                                                                    </tr>
                                                                    </tbody></table>
                                                            </td>
                                                        </tr>
                                                        </tbody></table></td>
                                            </tr>
                                            <tr>
                                                <td height="40" style="font-size:1px; line-height:1px;"> </td>
                                            </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                </tbody></table>
                            <!--[if mso]>
                            </td>
                            <![endif]-->

                            <!--[if mso]>
                            </tr>
                            </table>
                            <![endif]-->
                        </div></td>
                </tr><tr>

                    <td align="center" valign="top" bgcolor="#ffffff" style="background-color:#ffffff;">

                        <div>
                            <!--[if mso]>
                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                            <![endif]-->

                            <!--[if mso]>
                            <td valign="top" width="590" style="width:590px;">
                            <![endif]-->

                            <!--[if mso]>
                            </td>
                            <![endif]-->

                            <!--[if mso]>
                            </tr>
                            </table>
                            <![endif]-->
                        </div></td>
                </tr><tr>

                    <td align="center" valign="top" bgcolor="#ffffff" style="background-color:#ffffff;">

                        <div>
                            <!--[if mso]>
                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                            <![endif]-->

                            <!--[if mso]>
                            <td valign="top" width="590" style="width:590px;">
                            <![endif]-->
                            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#ffffff" style="min-width:100%; background-color:#ffffff;" name="Layout_5">
                                <tbody><tr>
                                    <td class="rnb-del-min-width" align="center" valign="top" bgcolor="#ffffff" style="background-color: #ffffff;">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px;">

                                            <tbody><tr>
                                                <td height="30" style="font-size:1px; line-height:1px;"> </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="rnb-container-padding" bgcolor="#ffffff" style="background-color: #ffffff;" align="left">

                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                        <tbody><tr>
                                                            <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                    <tbody><tr>
                                                                        <td style="font-size:16px; font-family:'Arial',Helvetica,sans-serif, sans-serif; color:#999; line-height: 21px;"><div style="line-height:150%;"><span style="color:#ffb119;"><span style="font-size:18px;"><strong>Olá {{$userName}},</strong></span></span></div>

                                                                            <div style="line-height:150%;"> </div>
                                                                            <p>
                                                                                <b>Parabéns!</b> Recebemos seu cadastro.
                                                                                Nessa plataforma você irá participar de leilões de combustíveis, acesse sua conta e faça bons negócios.
                                                                            </p>
                                                                            <br>
                                                                            <p>
                                                                                Para ter acesso a nossa plataforma basta acessar o link abaixo e criar o seu primeiro login.
                                                                            </p>
                                                                            <br>
{{--                                                                            <div style="line-height:150%;">Clique aqui.</div>--}}
                                                                            <p>
                                                                                Seja bem-vindo e bons negócios!
                                                                            </p>
                                                                            <div style="line-height:150%;"> </div>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody></table>

                                                            </td></tr>
                                                        </tbody></table></td>
                                            </tr>
                                            <tr>
                                                <td height="30" style="font-size:1px; line-height:1px;"> </td>
                                            </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                </tbody></table><!--[if mso]>
                            </td>
                            <![endif]-->

                            <!--[if mso]>
                            </tr>
                            </table>
                            <![endif]-->

                        </div></td>
                </tr><tr>

                    <td align="center" valign="top" bgcolor="#ffffff" style="background-color:#ffffff;">

                        <div>
                            <!--[if mso]>
                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                                <tr>
                            <![endif]-->

                            <!--[if mso]>
                            <td valign="top" width="590" style="width:590px;">
                            <![endif]-->
                            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#ffffff" style="min-width:590px; background-color:#ffffff;" name="Layout_9" id="Layout_9">
                                <tbody><tr>
                                    <td class="rnb-del-min-width" align="center" valign="top" bgcolor="#ffffff" style="min-width:590px; background-color: #ffffff;">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="mso-button-block rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate;">
                                            <tbody><tr>
                                                <td height="20" style="font-size:1px; line-height:1px;"> </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="rnb-container-padding" bgcolor="#ffffff" style="background-color: #ffffff;" align="left">

                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                        <tbody><tr>
                                                            <td class="rnb-force-col" valign="top">

                                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="550" align="center" class="rnb-col-1">

                                                                    <tbody><tr>
                                                                        <td valign="top">
                                                                            <table cellpadding="0" border="0" align="center" cellspacing="0" class="rnb-btn-col-content" style="margin:auto; border-collapse: separate;">
                                                                                <tbody><tr>
                                                                                    <td width="auto" valign="middle" bgcolor="#ffb119" align="center" height="40" style="font-size:18px; font-family:Arial,Helvetica,sans-serif; color:#ffffff; font-weight:normal; padding-left:20px; padding-right:20px; vertical-align: middle; background-color:#ffb119;border-radius:4px;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;">
                                                                        <span style="color:#ffffff; font-weight:normal;">
                                                                                <a style="text-decoration:none; color:#ffffff; font-weight:normal;" target="_blank" href="{{$url}}">Clique aqui</a>
                                                                            </span>
                                                                                    </td>
                                                                                </tr></tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody></table>
                                                            </td>
                                                        </tr>
                                                        </tbody></table></td>
                                            </tr>
                                            <tr>
                                                <td height="20" style="font-size:1px; line-height:1px;"> </td>
                                            </tr>
                                            </tbody></table>

                                    </td>
                                </tr>
                                </tbody></table>
                            <!--[if mso]>
                            </td>
                            <![endif]-->

                            <!--[if mso]>
                            </tr>
                            </table>
                            <![endif]-->
                        </div></td>
                </tr></tbody></table>
            <!--[if gte mso 9]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    </tbody></table>

</body></html>

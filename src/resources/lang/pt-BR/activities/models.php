<?php

return [
    'task' => [
        'model'           => 'tarefa',
        'title'           => 'título',
        'description'     => 'descrição',
        'start_date'      => 'data de início',
        'deadline'        => 'prazo',
        'status'          => 'status',
        'created_at'      => 'criado em',
        'updated_at'      => 'atualizado em',
        'deleted_at'      => 'removido em',
        'project_id'      => 'projeto',
        'user_id'         => 'usuário',
    ],

    'subtask' => [
        'model'           => 'tarefa',
        'title'           => 'título',
        'description'     => 'descrição',
        'start_date'      => 'data de início',
        'deadline'        => 'prazo',
        'status'          => 'status',
        'created_at'      => 'criado em',
        'updated_at'      => 'atualizado em',
        'deleted_at'      => 'removido em',
        'task_id'         => 'tarefa',
        'open_status_id'  => 'status',
        'team_id'         => 'equipe',
        'user_id'         => 'usuário',
    ]
];
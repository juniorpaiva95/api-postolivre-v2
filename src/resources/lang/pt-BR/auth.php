<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Essas credenciais não correspondem aos nossos registros.',
    'throttle' => 'Muitas tentativas de login. Tente novamente em :seconds segundos.',
    'invalid_credentials' => 'Você não tem acesso ao PostoLivre no momento. Por favor, entre em contato com seu administrador para configurar as permissões necessárias em seu perfil.',
    'unauthorized' => 'Você não tem autorização para acessar esta funcionalidade.',
    'unauthenticated' => 'Ops!! Parece que você não está autenticado.'
];

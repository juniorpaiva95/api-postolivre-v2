<?php

return [
    'generic' => 'Ops! Ocorreu um erro.',
    'no_record_found' => 'Nenhum registro encontrado',
    'not_found_http_exception' => 'Não encontramos resposta para esse tipo de ação',
    'resource_does_not_exist' => 'Nenhum resultado encontrado para o :model - :key',
    'resource_already_exist' => 'Recurso já existe',
    'validation_error' => 'Erro de validação',
    'not_confirmed_user' => 'Sua conta precisa ser confirmada. Enviamos instruções de como faze-lo para o seu email.',
    'there_is_entity_named' => 'Não existe :entity nomeada como :name',
    'there_is_entity_id' => 'Não existe :entity com o id :id',
    'user' => 'usuário',
];

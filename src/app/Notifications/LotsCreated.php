<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Collection;

class LotsCreated extends Notification
{
    use Queueable;

    private $lotes;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Collection $lotes)
    {
        $this->lotes = $lotes;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database'];
    }

    public function toDatabase($notifiable)
    {
        return [
            'lotes_id' => collect($this->lotes)->each(function ($lote) {
                return $lote->id;
            }),
            'title' => 'Lotes Gerados',
            'message' => $this->getMessage()
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'lotes_id' => collect($this->lotes)->each(function ($lote) {
                    return $lote->id;
                }),
                'title' => 'Lotes Gerados',
                'message' => $this->getMessage()
            ],
            'created_at' => Carbon::now()
        ]);
    }

    private function getMessage()
    {
        return $this->lotes->count() > 1 ?
            "{$this->lotes->count()} lotes acabam de ser gerados.\nCorra e garanta seu lance!"
            : "1 lote acaba de ser gerado.\nCorra e garanta seu lance!";
    }
}

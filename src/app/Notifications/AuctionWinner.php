<?php

namespace App\Notifications;

use Api\Bid\Models\Bid;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AuctionWinner extends Notification
{
    use Queueable;

    private $bidWinner;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Bid $bidWinner)
    {
        $this->bidWinner = $bidWinner;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toBroadcast($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'title' => 'Ganhador do Leilão',
                'lot'   => $this->bidWinner->lot,
                'message' => "Parabéns! Você foi o vencedor do lote #{$this->bidWinner->lot->id}"
            ],
            'created_at' => Carbon::now()
        ];
    }

    public function toDatabase() {
        return [
            'title' => 'Ganhador do Leilão',
            'lot'   => $this->bidWinner->lot,
            'message' => "Parabéns! Você foi o vencedor do lote #{$this->bidWinner->lot->id}"
        ];
    }
}

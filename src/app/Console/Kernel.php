<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule
            ->command('lots:generator')
            ->dailyAt('17:00')
//                ->everyMinute()
            ->before(function () {
                //Caso de uso>> Enviar e-mail para postos e distribuidoras sobre o fechamento dos lotes
            })
            ->onSuccess(function () {
                Log::alert("SUCESSO, LOTES GERADOS");
                //Caso de uso>> Enviar e-mail para o admin para notificar o fechamento de lotes com sucesso
            })
            ->onFailure(function () {
                Log::alert("ERROR NO LOTS GENERATOR");
            })
        ;

        $schedule
            ->command('notify:opening_of_auctions')
            ->dailyAt('12:00')
        ;

        $schedule
            ->command('notify:auction_close')
            ->dailyAt('16:30')
        ;

        $schedule
            ->command('notify:auction_winner')
            ->dailyAt('09:00')
            ;

        $schedule
            ->command('notify:payment_submission_request')
            ->dailyAt('13:00')
            ;
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

<?php

namespace App\Console\Commands;

use Api\Auction\Interfaces\IAuctionRepository;
use Api\Auction\Interfaces\IAuctionService;
use Api\Lot\Interfaces\ILotService;
use Api\User\Interfaces\IUserRepository;
use Api\User\Interfaces\IUserService;
use App\Notifications\LotsCreated;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class LotsGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lots:generator';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates daily batches | Cria os lotes diarios';

    private $lotService;
    private $userRepository;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        ILotService $lotService,
        IUserRepository $userRepository
    )
    {
        $this->lotService = $lotService;
        $this->userRepository = $userRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lotes = $this->lotService->gerarLoteDiario();
        $count = count($lotes);

        $users = $this->userRepository->scopeQuery(function ($query) {
            return $query->whereHas('distributor');
        })->get();

        Notification::send($users, new LotsCreated($lotes));

        Log::debug("[Gerar Lote] - {$count} lotes foram gerados!");
//        dd($lotes);
    }
}

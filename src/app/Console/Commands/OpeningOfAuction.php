<?php

namespace App\Console\Commands;

use Api\User\Interfaces\IUserRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class OpeningOfAuction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:opening_of_auctions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifica a abertura do leilão';

    private $userRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(IUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = $this->userRepository->all();

        Notification::send($users, new \App\Notifications\OpeningOfAuction());
    }
}
